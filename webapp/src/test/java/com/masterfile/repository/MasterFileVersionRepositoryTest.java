package com.masterfile.repository;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.masterfile.BaseTestClass;
import com.masterfile.domain.MasterFileVO;
import com.masterfile.domain.MasterFileVersionVO;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MasterFileVersionRepositoryTest extends BaseTestClass {

	@Autowired
	private MasterFileRepository masterFileRepository;
	
	@Autowired
	private MasterFileVersionRepository masterFileVersionRepository;
	
	private MasterFileVO masterFileVO;
	private MasterFileVersionVO masterFileVersionVO;
	
	@Before
	public void setUp() {
		masterFileVO = getMasterFileVO();
		masterFileRepository.save(masterFileVO);
		
		masterFileVersionVO = getMasterFileVersionVO();
	    masterFileVersionVO.setMasterFile(masterFileVO);
	    masterFileVersionRepository.save(masterFileVersionVO);
	}
	
	@Test
	public void testSaveAndFindMasterFileVersionVO() {
	    MasterFileVersionVO foundMasterFileVersionVO = masterFileVersionRepository.findById(masterFileVersionVO.getId()).orElse(null);
	    assertThat(masterFileVersionVO.getFileName()).isEqualTo(foundMasterFileVersionVO.getFileName());
	}

	@Test
	public void testFindAll() {
	    List<MasterFileVersionVO> masterFileVersionVOList = new LinkedList<>();
	    masterFileVersionRepository.findAll().forEach(masterFileVersionVOList::add);
	    assertThat(masterFileVersionVOList.size()).isGreaterThan(0);
	}
	
	@Test
	public void testUpdateMasterFileVersionVOByObject() {
	    String originalFileName = masterFileVersionVO.getFileName();
	    MasterFileVersionVO foundMasterFileVersionVO = masterFileVersionRepository.findById(masterFileVersionVO.getId()).get();
	    assertThat(masterFileVersionVO.getFileName()).isEqualTo(foundMasterFileVersionVO.getFileName());
	    
	    foundMasterFileVersionVO.setFileName("New File Name" + System.currentTimeMillis());
	    
	    MasterFileVersionVO updatedMasterFileVersionVO = masterFileVersionRepository.save(foundMasterFileVersionVO);
	    
	    foundMasterFileVersionVO = masterFileVersionRepository.findById(masterFileVersionVO.getId()).orElse(null);
	    assertThat(originalFileName).isNotEqualTo(updatedMasterFileVersionVO.getFileName());
	    assertThat(foundMasterFileVersionVO.getFileName()).isEqualTo(updatedMasterFileVersionVO.getFileName());
	}
	
	@Test
	public void testDeleteMasterFileVersionVOByObject() {
	    MasterFileVersionVO foundMasterFileVersionVO = masterFileVersionRepository.findById(masterFileVersionVO.getId()).get();
	    assertThat(masterFileVersionVO.getFileName()).isEqualTo(foundMasterFileVersionVO.getFileName());
	    	    
	    masterFileVersionRepository.delete(foundMasterFileVersionVO);
	    
	    foundMasterFileVersionVO = masterFileVersionRepository.findById(masterFileVersionVO.getId()).orElse(null);
	    assertThat(foundMasterFileVersionVO).isNull();
	}
	
	@Test
	public void testDeleteMasterFileVersionVOById() {
	    MasterFileVersionVO foundMasterFileVersionVO = masterFileVersionRepository.findById(masterFileVersionVO.getId()).get();
	    assertThat(masterFileVersionVO.getFileName()).isEqualTo(foundMasterFileVersionVO.getFileName());
	    
	    
	    masterFileVersionRepository.deleteById(foundMasterFileVersionVO.getId());
	    
	    foundMasterFileVersionVO = masterFileVersionRepository.findById(masterFileVersionVO.getId()).orElse(null);
	    assertThat(foundMasterFileVersionVO).isNull();
	}
	
	@Test
	public void testFindByMasterFileIdAndLatest() {
		Long newVersionId = 2L;
		masterFileVersionVO.setVersionId(newVersionId);
		masterFileVersionVO.setMasterFile(masterFileVO);
		masterFileVersionVO.setIsLatestVersion(true);
		
		MasterFileVersionVO updatedMasterFileVersionVO = masterFileVersionRepository.save(masterFileVersionVO);
		assertThat(updatedMasterFileVersionVO).isNotNull();

	    MasterFileVersionVO foundMasterFileVersionVO = masterFileVersionRepository.findByMasterFileIdAndLatest(masterFileVO.getId(), true);
	    assertThat(foundMasterFileVersionVO).isNotNull();
	    assertThat(foundMasterFileVersionVO.getVersionId()).isEqualTo(newVersionId);
	}
	
	@Test
	public void testFindByMasterFileIdAndVersion() {
		Long newVersionId = 2L;
		masterFileVersionVO.setVersionId(newVersionId);
		masterFileVersionVO.setMasterFile(masterFileVO);
		masterFileVersionVO.setIsLatestVersion(true);
		
		MasterFileVersionVO updatedMasterFileVersionVO = masterFileVersionRepository.save(masterFileVersionVO);
		assertThat(updatedMasterFileVersionVO).isNotNull();
		
	    MasterFileVersionVO foundMasterFileVersionVO = masterFileVersionRepository.findByMasterFileIdAndVersion(masterFileVO.getId(), newVersionId);
	    assertThat(foundMasterFileVersionVO).isNotNull();
	    assertThat(foundMasterFileVersionVO.getVersionId()).isEqualTo(newVersionId);
	}
	
	@Test
	public void testUpdateIsNotLatest() {	
		masterFileVersionVO.setIsLatestVersion(false);
	    Integer rowsUpdated = masterFileVersionRepository.updateIsNotLatest(masterFileVO.getId());
	    assertThat(rowsUpdated).isNotNull();
	    
	    MasterFileVersionVO foundMasterFileVersionVO = masterFileVersionRepository.findByMasterFileIdAndVersion(masterFileVO.getId(), masterFileVersionVO.getVersionId());
	    assertThat(foundMasterFileVersionVO).isNotNull();
	    assertThat(foundMasterFileVersionVO.getIsLatestVersion()).isEqualTo(false);
	}
	
	@Test
	public void testUpdateIsLatest() {	
	    Integer rowsUpdated = masterFileVersionRepository.updateIsLatest(masterFileVO.getId(), masterFileVersionVO.getVersionId());
	    assertThat(rowsUpdated).isNotNull();
	    
	    MasterFileVersionVO foundMasterFileVersionVO = masterFileVersionRepository.findByMasterFileIdAndVersion(masterFileVO.getId(), masterFileVersionVO.getVersionId());
	    assertThat(foundMasterFileVersionVO).isNotNull();
	    assertThat(foundMasterFileVersionVO.getIsLatestVersion()).isEqualTo(true);
	}
	@Test
	public void testGetAllFilesLatestBoolean() {	
	    List<MasterFileVersionVO> foundMasterFileVersionVOList = masterFileVersionRepository.getAllFiles(true);
	    assertThat(foundMasterFileVersionVOList).isNotNull();
	    assertThat(foundMasterFileVersionVOList.size()).isGreaterThan(0);
	}
	@Test
	public void testGetAllFilesLatestMasterFileId() {	
	    List<MasterFileVersionVO> foundMasterFileVersionVOList = masterFileVersionRepository.getAllFiles(masterFileVO.getId());
	    assertThat(foundMasterFileVersionVOList).isNotNull();
	    assertThat(foundMasterFileVersionVOList.size()).isGreaterThan(0);
	}
	@Test
	public void testDeleteMasterFileVersion() {	
	    Integer rowsUpdated = masterFileVersionRepository.deleteMasterFileVersion(masterFileVO.getId(), masterFileVersionVO.getVersionId());
	    assertThat(rowsUpdated).isNotNull();
	    assertThat(rowsUpdated).isGreaterThan(0);
	    
	    MasterFileVersionVO foundMasterFileVersionVO = masterFileVersionRepository.findByMasterFileIdAndVersion(masterFileVO.getId(), masterFileVersionVO.getVersionId());
	    assertThat(foundMasterFileVersionVO).isNull();
	    
	}
}
