package com.masterfile.repository;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.masterfile.BaseTestClass;
import com.masterfile.domain.MasterFileVO;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MasterFileRepositoryTest extends BaseTestClass {

	@Autowired
	private MasterFileRepository masterFileRepository;
	
	private MasterFileVO masterFileVO;
	
	@Before
	public void setUp() {
		masterFileVO = getMasterFileVO();
		masterFileRepository.save(masterFileVO);
	}
	
	@Test
	public void testSaveAndFindMasterFileVO() {
	    MasterFileVO foundMasterFileVO = masterFileRepository.findById(masterFileVO.getId()).orElse(null);
	    assertThat(masterFileVO.getOriginalFileName()).isEqualTo(foundMasterFileVO.getOriginalFileName());
	}

	@Test
	public void testFindAll() {
	    List<MasterFileVO> masterFileVOList = new LinkedList<>();
	    masterFileRepository.findAll().forEach(masterFileVOList::add);
	    assertThat(masterFileVOList.size()).isGreaterThan(0);
	}
	
	@Test
	public void testUpdateMasterFileVOByObject() {
	    String originalFileName = masterFileVO.getOriginalFileName();
	    MasterFileVO foundMasterFileVO = masterFileRepository.findById(masterFileVO.getId()).get();
	    assertThat(masterFileVO.getOriginalFileName()).isEqualTo(foundMasterFileVO.getOriginalFileName());
	    
	    foundMasterFileVO.setOriginalFileName("New File Name" + System.currentTimeMillis());
	    
	    MasterFileVO updatedMasterFileVO = masterFileRepository.save(foundMasterFileVO);
	    
	    foundMasterFileVO = masterFileRepository.findById(masterFileVO.getId()).orElse(null);
	    assertThat(originalFileName).isNotEqualTo(updatedMasterFileVO.getOriginalFileName());
	    assertThat(foundMasterFileVO.getOriginalFileName()).isEqualTo(updatedMasterFileVO.getOriginalFileName());
	}
	
	@Test
	public void testDeleteMasterFileVOByObject() {
	    MasterFileVO foundMasterFileVO = masterFileRepository.findById(masterFileVO.getId()).get();
	    assertThat(masterFileVO.getOriginalFileName()).isEqualTo(foundMasterFileVO.getOriginalFileName());
	    
	    
	    masterFileRepository.delete(foundMasterFileVO);
	    
	    foundMasterFileVO = masterFileRepository.findById(masterFileVO.getId()).orElse(null);
	    assertThat(foundMasterFileVO).isNull();
	}
	
	@Test
	public void testDeleteMasterFileVOById() {
	    MasterFileVO foundMasterFileVO = masterFileRepository.findById(masterFileVO.getId()).get();
	    assertThat(masterFileVO.getOriginalFileName()).isEqualTo(foundMasterFileVO.getOriginalFileName());
	    	    
	    masterFileRepository.deleteById(foundMasterFileVO.getId());
	    
	    foundMasterFileVO = masterFileRepository.findById(masterFileVO.getId()).orElse(null);
	    assertThat(foundMasterFileVO).isNull();
	}
}
