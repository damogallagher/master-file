package com.masterfile.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.masterfile.BaseTestClass;
import com.masterfile.domain.MasterFileVO;
import com.masterfile.domain.MasterFileVersionVO;
import com.masterfile.service.impl.FileServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class FileServiceTest extends BaseTestClass {

	@InjectMocks
	FileServiceImpl fileService = new FileServiceImpl();

	@Mock
	IMasterFileService mockMasterFileService;

	@Mock
	IMasterFileVersionService mockMasterFileVersionService;

	@After
	public void reset() {
		Mockito.reset(mockMasterFileService, mockMasterFileVersionService);
	}

	@Test
	public void testSaveFile_MultipartFileIsNull() {

		Exception exc = null;
		try {
			MultipartFile multiPartFile = null;
			MasterFileVersionVO masterFileVersion = fileService.saveFile(multiPartFile);
			assertNull(masterFileVersion);
		} catch (Exception e) {
			exc = e;
		}
		assertNull(exc);
	}

	@Test
	public void testSaveFile_MultipartFileIsEmpty() {

		Exception exc = null;
		try {
			byte[] content = null;
			MultipartFile multiPartFile = new MockMultipartFile("name", content);
			MasterFileVersionVO masterFileVersion = fileService.saveFile(multiPartFile);
			assertNull(masterFileVersion);
		} catch (Exception e) {
			exc = e;
		}
		assertNull(exc);
	}

	@Test
	public void testSaveFile_FailedToSaveMasterFile() {
		Exception exc = null;
		try {
			byte[] content = "content".getBytes();
			MultipartFile multiPartFile = new MockMultipartFile("name", content);

			MasterFileVO masterFileVO = null;
			Mockito.when(mockMasterFileService.saveMasterFile(Mockito.isA(MultipartFile.class)))
					.thenReturn(masterFileVO);

			MasterFileVersionVO masterFileVersion = fileService.saveFile(multiPartFile);
			assertNull(masterFileVersion);

			Mockito.verify(mockMasterFileService, Mockito.times(1)).saveMasterFile(Mockito.isA(MultipartFile.class));
		} catch (Exception e) {
			exc = e;
		}
		assertNotNull(exc);
	}

	@Test
	public void testSaveFile_FailedToSaveMasterFileVersion() {
		Exception exc = null;
		try {
			byte[] content = "content".getBytes();
			MultipartFile multiPartFile = new MockMultipartFile("name", content);

			MasterFileVO masterFileVO = getMasterFileVO();
			Mockito.when(mockMasterFileService.saveMasterFile(Mockito.isA(MultipartFile.class)))
					.thenReturn(masterFileVO);
			MasterFileVersionVO masterFileVersionVO = null;
			Mockito.when(mockMasterFileVersionService.saveMasterFileVersion(Mockito.isA(MultipartFile.class),
					Mockito.isA(MasterFileVO.class))).thenReturn(masterFileVersionVO);

			MasterFileVersionVO masterFileVersion = fileService.saveFile(multiPartFile);
			assertNull(masterFileVersion);

			Mockito.verify(mockMasterFileService, Mockito.times(1)).saveMasterFile(Mockito.isA(MultipartFile.class));
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1))
					.saveMasterFileVersion(Mockito.isA(MultipartFile.class), Mockito.isA(MasterFileVO.class));
		} catch (Exception e) {
			exc = e;
		}
		assertNotNull(exc);
	}

	@Test
	public void testSaveFile_Success() {
		Exception exc = null;
		try {
			byte[] content = "content".getBytes();
			MultipartFile multiPartFile = new MockMultipartFile("name", content);

			MasterFileVO masterFileVO = getMasterFileVO();
			Mockito.when(mockMasterFileService.saveMasterFile(Mockito.isA(MultipartFile.class)))
					.thenReturn(masterFileVO);
			MasterFileVersionVO masterFileVersionVO = getMasterFileVersionVO();
			Mockito.when(mockMasterFileVersionService.saveMasterFileVersion(Mockito.isA(MultipartFile.class),
					Mockito.isA(MasterFileVO.class))).thenReturn(masterFileVersionVO);

			MasterFileVersionVO masterFileVersion = fileService.saveFile(multiPartFile);
			assertNotNull(masterFileVersion);

			Mockito.verify(mockMasterFileService, Mockito.times(1)).saveMasterFile(Mockito.isA(MultipartFile.class));
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1))
					.saveMasterFileVersion(Mockito.isA(MultipartFile.class), Mockito.isA(MasterFileVO.class));
		} catch (Exception e) {
			exc = e;
		}
		assertNull(exc);
	}

	@Test
	public void testUpdateFile_MultipartFileIsNull() {

		Exception exc = null;
		try {
			Long masterFileId = 1L;
			MultipartFile multiPartFile = null;
			MasterFileVersionVO masterFileVersion = fileService.updateFile(multiPartFile, masterFileId);
			assertNull(masterFileVersion);
		} catch (Exception e) {
			exc = e;
		}
		assertNull(exc);
	}

	@Test
	public void testUpdateFile_MultipartFileIsEmpty() {

		Exception exc = null;
		try {
			Long masterFileId = 1L;
			byte[] content = null;
			MultipartFile multiPartFile = new MockMultipartFile("name", content);
			MasterFileVersionVO masterFileVersion = fileService.updateFile(multiPartFile, masterFileId);
			assertNull(masterFileVersion);
		} catch (Exception e) {
			exc = e;
		}
		assertNull(exc);
	}

	@Test
	public void testUpdateFile_MasterFileIdIsNull() {

		Exception exc = null;
		try {
			Long masterFileId = null;
			byte[] content = "content".getBytes();
			MultipartFile multiPartFile = new MockMultipartFile("name", content);
			MasterFileVersionVO masterFileVersion = fileService.updateFile(multiPartFile, masterFileId);
			assertNull(masterFileVersion);
		} catch (Exception e) {
			exc = e;
		}
		assertNull(exc);
	}

	@Test
	public void testUpdateFile_FailedToUpdateMasterFile() {
		Exception exc = null;
		try {
			Long masterFileId = 1L;
			byte[] content = "content".getBytes();
			MultipartFile multiPartFile = new MockMultipartFile("name", content);

			MasterFileVO masterFileVO = null;
			Mockito.when(mockMasterFileService.updateMasterFile(Mockito.isA(MultipartFile.class), Mockito.anyLong()))
					.thenReturn(masterFileVO);

			MasterFileVersionVO masterFileVersion = fileService.updateFile(multiPartFile, masterFileId);
			assertNull(masterFileVersion);

			Mockito.verify(mockMasterFileService, Mockito.times(1)).updateMasterFile(Mockito.isA(MultipartFile.class),
					Mockito.anyLong());
		} catch (Exception e) {
			exc = e;
		}
		assertNotNull(exc);
	}

	@Test
	public void testUpdateFile_FailedToUpdateMasterFileVersion() {
		Exception exc = null;
		try {
			Long masterFileId = 1L;
			byte[] content = "content".getBytes();
			MultipartFile multiPartFile = new MockMultipartFile("name", content);

			MasterFileVO masterFileVO = getMasterFileVO();
			Mockito.when(mockMasterFileService.updateMasterFile(Mockito.isA(MultipartFile.class), Mockito.anyLong()))
					.thenReturn(masterFileVO);
			MasterFileVersionVO masterFileVersionVO = null;
			Mockito.when(mockMasterFileVersionService.updateMasterFileVersion(Mockito.isA(MultipartFile.class),
					Mockito.isA(MasterFileVO.class))).thenReturn(masterFileVersionVO);

			MasterFileVersionVO masterFileVersion = fileService.updateFile(multiPartFile, masterFileId);
			assertNull(masterFileVersion);

			Mockito.verify(mockMasterFileService, Mockito.times(1)).updateMasterFile(Mockito.isA(MultipartFile.class),
					Mockito.anyLong());
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1))
					.updateMasterFileVersion(Mockito.isA(MultipartFile.class), Mockito.isA(MasterFileVO.class));
		} catch (Exception e) {
			exc = e;
		}
		assertNotNull(exc);
	}

	@Test
	public void testUpdateFile_Success() {
		Exception exc = null;
		try {
			Long masterFileId = 1L;
			byte[] content = "content".getBytes();
			MultipartFile multiPartFile = new MockMultipartFile("name", content);

			MasterFileVO masterFileVO = getMasterFileVO();
			Mockito.when(mockMasterFileService.updateMasterFile(Mockito.isA(MultipartFile.class), Mockito.anyLong()))
					.thenReturn(masterFileVO);
			MasterFileVersionVO masterFileVersionVO = getMasterFileVersionVO();
			Mockito.when(mockMasterFileVersionService.updateMasterFileVersion(Mockito.isA(MultipartFile.class),
					Mockito.isA(MasterFileVO.class))).thenReturn(masterFileVersionVO);

			MasterFileVersionVO masterFileVersion = fileService.updateFile(multiPartFile, masterFileId);
			assertNotNull(masterFileVersion);

			Mockito.verify(mockMasterFileService, Mockito.times(1)).updateMasterFile(Mockito.isA(MultipartFile.class),
					Mockito.anyLong());
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1))
					.updateMasterFileVersion(Mockito.isA(MultipartFile.class), Mockito.isA(MasterFileVO.class));
		} catch (Exception e) {
			exc = e;
		}
		assertNull(exc);
	}

	@Test
	public void testGetAllFiles_NullFilesReturned() {
		List<MasterFileVersionVO> returnedFiles = null;
		Mockito.when(mockMasterFileVersionService.getAllFiles()).thenReturn(returnedFiles);

		List<MasterFileVersionVO> masterFileVersions = fileService.getAllFiles();
		assertNull(masterFileVersions);

		Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getAllFiles();
	}

	@Test
	public void testGetAllFiles_NoFilesReturned() {
		List<MasterFileVersionVO> returnedFiles = new LinkedList<>();
		Mockito.when(mockMasterFileVersionService.getAllFiles()).thenReturn(returnedFiles);

		List<MasterFileVersionVO> masterFileVersions = fileService.getAllFiles();
		assertNotNull(masterFileVersions);
		assertTrue(masterFileVersions.size() == 0);

		Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getAllFiles();
	}

	@Test
	public void testGetAllFiles_1FileReturned() {
		List<MasterFileVersionVO> returnedFiles = new LinkedList<>();
		returnedFiles.add(getMasterFileVersionVO());
		Mockito.when(mockMasterFileVersionService.getAllFiles()).thenReturn(returnedFiles);

		List<MasterFileVersionVO> masterFileVersions = fileService.getAllFiles();
		assertNotNull(masterFileVersions);
		assertTrue(masterFileVersions.size() == 1);

		Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getAllFiles();
	}

	@Test
	public void testGetAllFiles_2FilesReturned() {
		List<MasterFileVersionVO> returnedFiles = new LinkedList<>();
		returnedFiles.add(getMasterFileVersionVO());
		returnedFiles.add(getMasterFileVersionVO());
		Mockito.when(mockMasterFileVersionService.getAllFiles()).thenReturn(returnedFiles);

		List<MasterFileVersionVO> masterFileVersions = fileService.getAllFiles();
		assertNotNull(masterFileVersions);
		assertTrue(masterFileVersions.size() == 2);

		Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getAllFiles();
	}

	@Test
	public void testGetLatestFileVersion_MasterFileIdIsNull() {
		Long masterFileId = null;

		MasterFileVersionVO returnedMasterFileVersionVO = fileService.getLatestFileVersion(masterFileId);
		assertNull(returnedMasterFileVersionVO);
	}

	@Test
	public void testGetLatestFileVersion_NullReturned() {
		MasterFileVersionVO masterFileVersionVO = null;
		Long masterFileId = 1L;
		Mockito.when(mockMasterFileVersionService.getMasterFileVersion(Mockito.anyLong()))
				.thenReturn(masterFileVersionVO);

		MasterFileVersionVO returnedMasterFileVersionVO = fileService.getLatestFileVersion(masterFileId);
		assertNull(returnedMasterFileVersionVO);

		Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getMasterFileVersion(Mockito.anyLong());
	}

	@Test
	public void testGetLatestFileVersion_Success() {
		MasterFileVersionVO masterFileVersionVO = getMasterFileVersionVO();
		Long masterFileId = 1L;
		Mockito.when(mockMasterFileVersionService.getMasterFileVersion(Mockito.anyLong()))
				.thenReturn(masterFileVersionVO);

		MasterFileVersionVO returnedMasterFileVersionVO = fileService.getLatestFileVersion(masterFileId);
		assertNotNull(returnedMasterFileVersionVO);

		Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getMasterFileVersion(Mockito.anyLong());
	}

	@Test
	public void testGetFileVersion_MasterFileIdIsNull() {
		Long masterFileId = null;
		Long versionId = 1L;
		MasterFileVersionVO returnedMasterFileVersionVO = fileService.getFileVersion(masterFileId, versionId);
		assertNull(returnedMasterFileVersionVO);
	}

	@Test
	public void testGetFileVersion_VersionIdIsNull() {
		Long masterFileId = 1L;
		Long versionId = null;
		MasterFileVersionVO returnedMasterFileVersionVO = fileService.getFileVersion(masterFileId, versionId);
		assertNull(returnedMasterFileVersionVO);
	}

	@Test
	public void testGetFileVersion_NullReturned() {
		MasterFileVersionVO masterFileVersionVO = null;
		Long masterFileId = 1L;
		Long versionId = 1L;
		Mockito.when(mockMasterFileVersionService.getMasterFileVersion(Mockito.anyLong(), Mockito.anyLong()))
				.thenReturn(masterFileVersionVO);

		MasterFileVersionVO returnedMasterFileVersionVO = fileService.getFileVersion(masterFileId, versionId);
		assertNull(returnedMasterFileVersionVO);

		Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getMasterFileVersion(Mockito.anyLong(),
				Mockito.anyLong());
	}

	@Test
	public void testGetFileVersion_Success() {
		Long versionId = 1L;
		MasterFileVersionVO masterFileVersionVO = getMasterFileVersionVO();
		Long masterFileId = 1L;
		Mockito.when(mockMasterFileVersionService.getMasterFileVersion(Mockito.anyLong(), Mockito.anyLong()))
				.thenReturn(masterFileVersionVO);

		MasterFileVersionVO returnedMasterFileVersionVO = fileService.getFileVersion(masterFileId, versionId);
		assertNotNull(returnedMasterFileVersionVO);

		Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getMasterFileVersion(Mockito.anyLong(),
				Mockito.anyLong());
	}

	@Test
	public void testGetAllFileVersions_MasterFileIdIsNull() {
		Long masterFileId = null;

		List<MasterFileVersionVO> returnedMasterFileVersionVO = fileService.getAllFileVersions(masterFileId);
		assertNull(returnedMasterFileVersionVO);
	}

	@Test
	public void testGetAllFileVersions_NullReturned() {
		Long masterFileId = 1L;
		List<MasterFileVersionVO> allFileVersions = null;

		Mockito.when(mockMasterFileVersionService.getAllFileVersions(Mockito.anyLong())).thenReturn(allFileVersions);

		List<MasterFileVersionVO> returnedMasterFileVersionVO = fileService.getAllFileVersions(masterFileId);
		assertNull(returnedMasterFileVersionVO);

		Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getAllFileVersions(Mockito.anyLong());
	}

	@Test
	public void testGetAllFileVersions_NoFilesReturned() {
		Long masterFileId = 1L;
		List<MasterFileVersionVO> allFileVersions = new LinkedList<>();

		Mockito.when(mockMasterFileVersionService.getAllFileVersions(Mockito.anyLong())).thenReturn(allFileVersions);

		List<MasterFileVersionVO> returnedMasterFileVersionVO = fileService.getAllFileVersions(masterFileId);
		assertNotNull(returnedMasterFileVersionVO);
		assertTrue(returnedMasterFileVersionVO.size() == 0);

		Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getAllFileVersions(Mockito.anyLong());
	}

	@Test
	public void testGetAllFileVersions_Success1File() {
		Long masterFileId = 1L;
		List<MasterFileVersionVO> allFileVersions = new LinkedList<>();
		allFileVersions.add(getMasterFileVersionVO());

		Mockito.when(mockMasterFileVersionService.getAllFileVersions(Mockito.anyLong())).thenReturn(allFileVersions);

		List<MasterFileVersionVO> returnedMasterFileVersionVO = fileService.getAllFileVersions(masterFileId);
		assertNotNull(returnedMasterFileVersionVO);
		assertTrue(returnedMasterFileVersionVO.size() == 1);

		Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getAllFileVersions(Mockito.anyLong());
	}

	@Test
	public void testGetAllFileVersions_Success2Files() {
		Long masterFileId = 1L;
		List<MasterFileVersionVO> allFileVersions = new LinkedList<>();
		allFileVersions.add(getMasterFileVersionVO());
		allFileVersions.add(getMasterFileVersionVO());

		Mockito.when(mockMasterFileVersionService.getAllFileVersions(Mockito.anyLong())).thenReturn(allFileVersions);

		List<MasterFileVersionVO> returnedMasterFileVersionVO = fileService.getAllFileVersions(masterFileId);
		assertNotNull(returnedMasterFileVersionVO);
		assertTrue(returnedMasterFileVersionVO.size() == 2);

		Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getAllFileVersions(Mockito.anyLong());
	}

	@Test
	public void testDeleteMasterFile_MasterFileIdIsNull() {
		Long masterFileId = null;

		Boolean result = fileService.deleteMasterFile(masterFileId);
		assertFalse(result);
	}

	@Test
	public void testDeleteMasterFile_FalseReturned() {
		Long masterFileId = 1L;

		Mockito.when(mockMasterFileService.deleteMasterFile(Mockito.anyLong())).thenReturn(false);

		Boolean result = fileService.deleteMasterFile(masterFileId);
		assertFalse(result);

		Mockito.verify(mockMasterFileService, Mockito.times(1)).deleteMasterFile(Mockito.anyLong());
	}

	@Test
	public void testDeleteMasterFile_Success() {
		Long masterFileId = 1L;

		Mockito.when(mockMasterFileService.deleteMasterFile(Mockito.anyLong())).thenReturn(true);

		Boolean result = fileService.deleteMasterFile(masterFileId);
		assertTrue(result);

		Mockito.verify(mockMasterFileService, Mockito.times(1)).deleteMasterFile(Mockito.anyLong());
	}

	@Test
	public void testDeleteMasterFileVersion_MasterFileIdIsNull() {
		Long masterFileId = null;
		Long versionId = 1L;

		Boolean result = fileService.deleteMasterFileVersion(masterFileId, versionId);
		assertFalse(result);
	}

	@Test
	public void testDeleteMasterFileVersion_VersionIdIsNull() {
		Long masterFileId = 1L;
		Long versionId = null;

		Boolean result = fileService.deleteMasterFileVersion(masterFileId, versionId);
		assertFalse(result);
	}

	@Test
	public void testDeleteMasterFileVersion_FailedToGetMasterFileVersion() {
		Long masterFileId = 1L;
		Long versionId = 1L;

		Exception exc = null;
		try {
			MasterFileVersionVO masterFileVersion = null;
			Mockito.when(mockMasterFileVersionService.getMasterFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(masterFileVersion);
			
			Boolean result = fileService.deleteMasterFileVersion(masterFileId, versionId);
			assertFalse(result);
			
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getMasterFileVersion(Mockito.anyLong(), Mockito.anyLong());
		} catch (Exception e) {
			exc = e;
		}
		assertNull(exc);
	}
	
	@Test
	public void testDeleteMasterFileVersion_NotLatestVersionFailedToDeleteMasterFileVersion() {
		Long masterFileId = 1L;
		Long versionId = 1L;

		Exception exc = null;
		try {
			MasterFileVersionVO masterFileVersion = getMasterFileVersionVO();
			masterFileVersion.setIsLatestVersion(false);
			Mockito.when(mockMasterFileVersionService.getMasterFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(masterFileVersion);
			Mockito.when(mockMasterFileVersionService.deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(false);
			
			Boolean result = fileService.deleteMasterFileVersion(masterFileId, versionId);
			assertFalse(result);
			
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getMasterFileVersion(Mockito.anyLong(), Mockito.anyLong());
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong());
		} catch (Exception e) {
			exc = e;
		}
		assertNotNull(exc);
	}
	
	@Test
	public void testDeleteMasterFileVersion_NotLatestVersionSuccess() {
		Long masterFileId = 1L;
		Long versionId = 1L;

		Exception exc = null;
		try {
			MasterFileVersionVO masterFileVersion = getMasterFileVersionVO();
			masterFileVersion.setIsLatestVersion(false);
			Mockito.when(mockMasterFileVersionService.getMasterFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(masterFileVersion);
			Mockito.when(mockMasterFileVersionService.deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(true);
			
			Boolean result = fileService.deleteMasterFileVersion(masterFileId, versionId);
			assertTrue(result);
			
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getMasterFileVersion(Mockito.anyLong(), Mockito.anyLong());
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong());
		} catch (Exception e) {
			exc = e;
		}
		assertNull(exc);
	}
	
	@Test
	public void testDeleteMasterFileVersion_LatestVersionFailedToDeleteMasterFileVersion() {
		Long masterFileId = 1L;
		Long versionId = 1L;

		Exception exc = null;
		try {
			MasterFileVersionVO masterFileVersion = getMasterFileVersionVO();
			masterFileVersion.setIsLatestVersion(true);
			Mockito.when(mockMasterFileVersionService.getMasterFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(masterFileVersion);
			Mockito.when(mockMasterFileVersionService.deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(false);
			
			Boolean result = fileService.deleteMasterFileVersion(masterFileId, versionId);
			assertFalse(result);
			
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getMasterFileVersion(Mockito.anyLong(), Mockito.anyLong());
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong());
		} catch (Exception e) {
			exc = e;
		}
		assertNotNull(exc);
	}
	
	@Test
	public void testDeleteMasterFileVersion_LatestVersionAllFileVersionsIsEmptyFailedToDeleteMasterFile() {
		Long masterFileId = 1L;
		Long versionId = 1L;

		Exception exc = null;
		try {
			MasterFileVersionVO masterFileVersion = getMasterFileVersionVO();
			masterFileVersion.setIsLatestVersion(true);
			List<MasterFileVersionVO> masterFileVersionList = new LinkedList<>();
			Mockito.when(mockMasterFileVersionService.getMasterFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(masterFileVersion);
			Mockito.when(mockMasterFileVersionService.deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(true);
			Mockito.when(mockMasterFileVersionService.getAllFileVersions(Mockito.anyLong())).thenReturn(masterFileVersionList);
			Mockito.when(mockMasterFileService.deleteMasterFile(Mockito.anyLong())).thenReturn(false);
			
			Boolean result = fileService.deleteMasterFileVersion(masterFileId, versionId);
			assertFalse(result);
			
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getMasterFileVersion(Mockito.anyLong(), Mockito.anyLong());
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong());
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getAllFileVersions(Mockito.anyLong());
			Mockito.verify(mockMasterFileService, Mockito.times(1)).deleteMasterFile(Mockito.anyLong());
		} catch (Exception e) {
			exc = e;
		}
		assertNotNull(exc);
	}
	@Test
	public void testDeleteMasterFileVersion_LatestVersionAllFileVersionsIsEmptySuccessfullyDeletedMasterFile() {
		Long masterFileId = 1L;
		Long versionId = 1L;

		Exception exc = null;
		try {
			MasterFileVersionVO masterFileVersion = getMasterFileVersionVO();
			masterFileVersion.setIsLatestVersion(true);
			List<MasterFileVersionVO> masterFileVersionList = new LinkedList<>();
			Mockito.when(mockMasterFileVersionService.getMasterFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(masterFileVersion);
			Mockito.when(mockMasterFileVersionService.deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(true);
			Mockito.when(mockMasterFileVersionService.getAllFileVersions(Mockito.anyLong())).thenReturn(masterFileVersionList);
			Mockito.when(mockMasterFileService.deleteMasterFile(Mockito.anyLong())).thenReturn(true);
			
			Boolean result = fileService.deleteMasterFileVersion(masterFileId, versionId);
			assertTrue(result);
			
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getMasterFileVersion(Mockito.anyLong(), Mockito.anyLong());
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong());
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getAllFileVersions(Mockito.anyLong());
			Mockito.verify(mockMasterFileService, Mockito.times(1)).deleteMasterFile(Mockito.anyLong());
		} catch (Exception e) {
			exc = e;
		}
		assertNull(exc);
	}
	
	@Test
	public void testDeleteMasterFileVersion_LatestVersionAllFileVersionsIsNotEmptyFailedToUpdateMasterFileVersionToLatest() {
		Long masterFileId = 1L;
		Long versionId = 1L;

		Exception exc = null;
		try {
			MasterFileVersionVO masterFileVersion = getMasterFileVersionVO();
			masterFileVersion.setIsLatestVersion(true);
			List<MasterFileVersionVO> masterFileVersionList = new LinkedList<>();
			masterFileVersionList.add(getMasterFileVersionVO());
			Mockito.when(mockMasterFileVersionService.getMasterFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(masterFileVersion);
			Mockito.when(mockMasterFileVersionService.deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(true);
			Mockito.when(mockMasterFileVersionService.getAllFileVersions(Mockito.anyLong())).thenReturn(masterFileVersionList);
			Mockito.when(mockMasterFileVersionService.updateMasterFileVersionToLatest(Mockito.anyLong(), Mockito.anyLong())).thenReturn(false);
			
			Boolean result = fileService.deleteMasterFileVersion(masterFileId, versionId);
			assertFalse(result);
			
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getMasterFileVersion(Mockito.anyLong(), Mockito.anyLong());
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong());
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getAllFileVersions(Mockito.anyLong());
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).updateMasterFileVersionToLatest(Mockito.anyLong(), Mockito.anyLong());
		} catch (Exception e) {
			exc = e;
		}
		assertNotNull(exc);
	}
	
	@Test
	public void testDeleteMasterFileVersion_LatestVersionAllFileVersionsIsNotEmptySuccessfullyUpdatedMasterFileVersionToLatest() {
		Long masterFileId = 1L;
		Long versionId = 1L;

		Exception exc = null;
		try {
			MasterFileVersionVO masterFileVersion = getMasterFileVersionVO();
			masterFileVersion.setIsLatestVersion(true);
			List<MasterFileVersionVO> masterFileVersionList = new LinkedList<>();
			masterFileVersionList.add(getMasterFileVersionVO());
			Mockito.when(mockMasterFileVersionService.getMasterFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(masterFileVersion);
			Mockito.when(mockMasterFileVersionService.deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(true);
			Mockito.when(mockMasterFileVersionService.getAllFileVersions(Mockito.anyLong())).thenReturn(masterFileVersionList);
			Mockito.when(mockMasterFileVersionService.updateMasterFileVersionToLatest(Mockito.anyLong(), Mockito.anyLong())).thenReturn(true);
			
			Boolean result = fileService.deleteMasterFileVersion(masterFileId, versionId);
			assertTrue(result);
			
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getMasterFileVersion(Mockito.anyLong(), Mockito.anyLong());
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong());
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).getAllFileVersions(Mockito.anyLong());
			Mockito.verify(mockMasterFileVersionService, Mockito.times(1)).updateMasterFileVersionToLatest(Mockito.anyLong(), Mockito.anyLong());
		} catch (Exception e) {
			exc = e;
		}
		assertNull(exc);
	}
}
