package com.masterfile.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataAccessException;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.masterfile.BaseTestClass;
import com.masterfile.domain.MasterFileVO;
import com.masterfile.domain.MasterFileVersionVO;
import com.masterfile.repository.MasterFileVersionRepository;
import com.masterfile.service.impl.MasterFileVersionServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class MasterFileVersionServiceTest extends BaseTestClass {

	@InjectMocks
	MasterFileVersionServiceImpl masterFileVersionService = new MasterFileVersionServiceImpl();

	@Mock
	MasterFileVersionRepository mockMasterFileVersionRepository;

	@Mock
	DataAccessException mockDataAccessException;
	@After
	public void reset() {
		Mockito.reset(mockMasterFileVersionRepository);
	}

	@Test
	public void testGetMasterFileVersion_MasterFileIdIsNull() {
		Long masterFileId = null;

		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.getMasterFileVersion(masterFileId);
		assertNull(returnedMasterFileVersionVO);
	}

	@Test
	public void testGetMasterFileVersion_NullReturned() {
		Long masterFileId = 1L;

		MasterFileVersionVO masterFileVersionVO = null;
		Mockito.when(mockMasterFileVersionRepository.findByMasterFileIdAndLatest(Mockito.anyLong(), Mockito.anyBoolean())).thenReturn(masterFileVersionVO );
		
		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.getMasterFileVersion(masterFileId);
		assertNull(returnedMasterFileVersionVO);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).findByMasterFileIdAndLatest(Mockito.anyLong(), Mockito.anyBoolean());
	}
	
	@Test
	public void testGetMasterFileVersion_Success() {
		Long masterFileId = 1L;

		MasterFileVersionVO masterFileVersionVO = getMasterFileVersionVO();
		Mockito.when(mockMasterFileVersionRepository.findByMasterFileIdAndLatest(Mockito.anyLong(), Mockito.anyBoolean())).thenReturn(masterFileVersionVO);
		
		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.getMasterFileVersion(masterFileId);
		assertNotNull(returnedMasterFileVersionVO);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).findByMasterFileIdAndLatest(Mockito.anyLong(), Mockito.anyBoolean());
	}
	
	@Test
	public void testGetMasterFileVersionWithVersionId_MasterFileIdIsNull() {
		Long masterFileId = null;
		Long versionId = 1L;
		
		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.getMasterFileVersion(masterFileId, versionId);
		assertNull(returnedMasterFileVersionVO);
	}
	@Test
	public void testGetMasterFileVersionWithVersionId_VersionIdIsNull() {
		Long masterFileId = 1L;
		Long versionId = null;
		
		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.getMasterFileVersion(masterFileId, versionId);
		assertNull(returnedMasterFileVersionVO);
	}
	@Test
	public void testGetMasterFileVersionWithVersionId_NullReturned() {
		Long masterFileId = 1L;
		Long versionId = 1L;
		
		MasterFileVersionVO masterFileVersionVO = null;
		Mockito.when(mockMasterFileVersionRepository.findByMasterFileIdAndVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(masterFileVersionVO );
		
		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.getMasterFileVersion(masterFileId, versionId);
		assertNull(returnedMasterFileVersionVO);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).findByMasterFileIdAndVersion(Mockito.anyLong(), Mockito.anyLong());
	}
	
	@Test
	public void testGetMasterFileVersionWithVersionId_Success() {
		Long masterFileId = 1L;
		Long versionId = 1L;
		
		MasterFileVersionVO masterFileVersionVO = getMasterFileVersionVO();
		Mockito.when(mockMasterFileVersionRepository.findByMasterFileIdAndVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(masterFileVersionVO );
		
		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.getMasterFileVersion(masterFileId, versionId);
		assertNotNull(returnedMasterFileVersionVO);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).findByMasterFileIdAndVersion(Mockito.anyLong(), Mockito.anyLong());
	}

	
	@Test
	public void testSaveFile_MultipartFileIsNull() {
		MultipartFile multiPartFile = null;
		MasterFileVO masterFile = getMasterFileVO();
		
		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.saveMasterFileVersion(multiPartFile, masterFile);
		assertNull(returnedMasterFileVersionVO);
	}

	@Test
	public void testSaveFile_MultipartFileIsEmpty() {
		byte[] content = null;
		MultipartFile multiPartFile = new MockMultipartFile("name", content);
		MasterFileVO masterFile = getMasterFileVO();
		
		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.saveMasterFileVersion(multiPartFile, masterFile);
		assertNull(returnedMasterFileVersionVO);
	}
	@Test
	public void testSaveFile_MasterFileVOIsNull() {
		byte[] content = "content".getBytes();
		MultipartFile multiPartFile = new MockMultipartFile("name", content);
		MasterFileVO masterFile = null;
		
		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.saveMasterFileVersion(multiPartFile, masterFile);
		assertNull(returnedMasterFileVersionVO);
	}
	
	@Test
	public void testSaveFile_FailedToSave() {
		byte[] content = "content".getBytes();
		MultipartFile multiPartFile = new MockMultipartFile("name", content);
		MasterFileVO masterFile = getMasterFileVO();
		
		MasterFileVersionVO masterFileVersionVO = null;
		Mockito.when(mockMasterFileVersionRepository.save(Mockito.isA(MasterFileVersionVO.class))).thenReturn(masterFileVersionVO);

		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.saveMasterFileVersion(multiPartFile, masterFile);
		assertNull(returnedMasterFileVersionVO);

		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).save(Mockito.isA(MasterFileVersionVO.class));
	}
	
	@Test
	public void testSaveFile_Success() {
		byte[] content = "content".getBytes();
		MultipartFile multiPartFile = new MockMultipartFile("name", content);
		MasterFileVO masterFile = getMasterFileVO();
		
		MasterFileVersionVO masterFileVersionVO = getMasterFileVersionVO();
		Mockito.when(mockMasterFileVersionRepository.save(Mockito.isA(MasterFileVersionVO.class))).thenReturn(masterFileVersionVO);

		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.saveMasterFileVersion(multiPartFile, masterFile);
		assertNotNull(returnedMasterFileVersionVO);

		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).save(Mockito.isA(MasterFileVersionVO.class));
	}
	@Test
	public void testSaveMasterFileVersion_MasterFileVersionVOIsNull() {
		MasterFileVersionVO masterFileVersionVO = null;

		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.saveMasterFileVersion(masterFileVersionVO);
		assertNull(returnedMasterFileVersionVO);
	}
	@Test
	public void testSaveMasterFileVersion_FailedToSave() {
		MasterFileVersionVO masterFileVersionVO = getMasterFileVersionVO();
		Mockito.when(mockMasterFileVersionRepository.save(Mockito.isA(MasterFileVersionVO.class))).thenReturn(null);

		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.saveMasterFileVersion(masterFileVersionVO);
		assertNull(returnedMasterFileVersionVO);

		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).save(Mockito.isA(MasterFileVersionVO.class));
	}
	@Test
	public void testSaveMasterFileVersion_Success() {
		MasterFileVersionVO masterFileVersionVO = getMasterFileVersionVO();
		Mockito.when(mockMasterFileVersionRepository.save(Mockito.isA(MasterFileVersionVO.class))).thenReturn(masterFileVersionVO);

		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.saveMasterFileVersion(masterFileVersionVO);
		assertNotNull(returnedMasterFileVersionVO);

		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).save(Mockito.isA(MasterFileVersionVO.class));
	}
	
	@Test
	public void testUpdateMasterFileVersion_MultipartFileIsNull() {
		MultipartFile multiPartFile = null;
		MasterFileVO masterFile = getMasterFileVO();
		
		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.updateMasterFileVersion(multiPartFile, masterFile);
		assertNull(returnedMasterFileVersionVO);
	}

	@Test
	public void testUpdateMasterFileVersion_MultipartFileIsEmpty() {
		byte[] content = null;
		MultipartFile multiPartFile = new MockMultipartFile("name", content);
		MasterFileVO masterFile = getMasterFileVO();
		
		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.updateMasterFileVersion(multiPartFile, masterFile);
		assertNull(returnedMasterFileVersionVO);
	}
	
	@Test
	public void testUpdateMasterFileVersion_MasterFileVOIsNull() {
		byte[] content = "content".getBytes();
		MultipartFile multiPartFile = new MockMultipartFile("name", content);
		MasterFileVO masterFile = null;
		
		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.updateMasterFileVersion(multiPartFile, masterFile);
		assertNull(returnedMasterFileVersionVO);
	}
	
	@Test
	public void testUpdateMasterFileVersion_FailedToGetMasterFileVersion() {
		byte[] content = "content".getBytes();
		MultipartFile multiPartFile = new MockMultipartFile("name", content);
		MasterFileVO masterFile = getMasterFileVO();
		masterFile.setId(1L);
		
		MasterFileVersionVO masterFileVersionVO = null;
		Mockito.when(mockMasterFileVersionRepository.findByMasterFileIdAndLatest(Mockito.anyLong(), Mockito.anyBoolean())).thenReturn(masterFileVersionVO);
		
		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.updateMasterFileVersion(multiPartFile, masterFile);
		assertNull(returnedMasterFileVersionVO);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).findByMasterFileIdAndLatest(Mockito.anyLong(), Mockito.anyBoolean());
	}
	
	@Test
	public void testUpdateMasterFileVersion_FailedToUpdateNotLatest() {
		byte[] content = "content".getBytes();
		MultipartFile multiPartFile = new MockMultipartFile("name", content);
		MasterFileVO masterFile = getMasterFileVO();
		masterFile.setId(1L);
		
		MasterFileVersionVO masterFileVersionVO = getMasterFileVersionVO();
		Mockito.when(mockMasterFileVersionRepository.findByMasterFileIdAndLatest(Mockito.anyLong(), Mockito.anyBoolean())).thenReturn(masterFileVersionVO);
		Mockito.when(mockMasterFileVersionRepository.updateIsNotLatest(Mockito.anyLong())).thenReturn(0);
		
		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.updateMasterFileVersion(multiPartFile, masterFile);
		assertNull(returnedMasterFileVersionVO);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).findByMasterFileIdAndLatest(Mockito.anyLong(), Mockito.anyBoolean());
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).updateIsNotLatest(Mockito.anyLong());
	}
	
	@Test
	public void testUpdateMasterFileVersion_FailedToSave() {
		byte[] content = "content".getBytes();
		MultipartFile multiPartFile = new MockMultipartFile("name", content);
		MasterFileVO masterFile = getMasterFileVO();
		masterFile.setId(1L);
		
		MasterFileVersionVO masterFileVersionVO = getMasterFileVersionVO();
		Mockito.when(mockMasterFileVersionRepository.findByMasterFileIdAndLatest(Mockito.anyLong(), Mockito.anyBoolean())).thenReturn(masterFileVersionVO);
		Mockito.when(mockMasterFileVersionRepository.updateIsNotLatest(Mockito.anyLong())).thenReturn(1);
		Mockito.when(mockMasterFileVersionRepository.save(Mockito.isA(MasterFileVersionVO.class))).thenReturn(null);
		
		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.updateMasterFileVersion(multiPartFile, masterFile);
		assertNull(returnedMasterFileVersionVO);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).findByMasterFileIdAndLatest(Mockito.anyLong(), Mockito.anyBoolean());
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).updateIsNotLatest(Mockito.anyLong());
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).save(Mockito.isA(MasterFileVersionVO.class));
	}
	
	@Test
	public void testUpdateMasterFileVersion_Success() {
		byte[] content = "content".getBytes();
		MultipartFile multiPartFile = new MockMultipartFile("name", content);
		MasterFileVO masterFile = getMasterFileVO();
		masterFile.setId(1L);
		
		MasterFileVersionVO masterFileVersionVO = getMasterFileVersionVO();
		Mockito.when(mockMasterFileVersionRepository.findByMasterFileIdAndLatest(Mockito.anyLong(), Mockito.anyBoolean())).thenReturn(masterFileVersionVO);
		Mockito.when(mockMasterFileVersionRepository.updateIsNotLatest(Mockito.anyLong())).thenReturn(1);
		Mockito.when(mockMasterFileVersionRepository.save(Mockito.isA(MasterFileVersionVO.class))).thenReturn(masterFileVersionVO);
		
		MasterFileVersionVO returnedMasterFileVersionVO = masterFileVersionService.updateMasterFileVersion(multiPartFile, masterFile);
		assertNotNull(returnedMasterFileVersionVO);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).findByMasterFileIdAndLatest(Mockito.anyLong(), Mockito.anyBoolean());
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).updateIsNotLatest(Mockito.anyLong());
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).save(Mockito.isA(MasterFileVersionVO.class));
	}
	
	@Test
	public void testUpdateMasterFileVersionToLatest_MasterFileIdIsNull() {
		Long masterFileId = null;
		Long previousVersionId = 1L;
		Boolean result = masterFileVersionService.updateMasterFileVersionToLatest(masterFileId, previousVersionId);
		assertFalse(result);
	}
	
	@Test
	public void testUpdateMasterFileVersionToLatest_VersionIdIsNull() {
		Long masterFileId = 1L;
		Long previousVersionId = null;
		Boolean result = masterFileVersionService.updateMasterFileVersionToLatest(masterFileId, previousVersionId);
		assertFalse(result);
	}
	
	@Test
	public void testUpdateMasterFileVersionToLatest_FailedToUpdateAsLatest() {
		Long masterFileId = 1L;
		Long previousVersionId = 1L;
		
		Mockito.when(mockMasterFileVersionRepository.updateIsLatest(Mockito.anyLong(), Mockito.anyLong())).thenReturn(0);
		
		Boolean result = masterFileVersionService.updateMasterFileVersionToLatest(masterFileId, previousVersionId);
		assertFalse(result);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).updateIsLatest(Mockito.anyLong(), Mockito.anyLong());
	}
	
	@Test
	public void testUpdateMasterFileVersionToLatest_Success() {
		Long masterFileId = 1L;
		Long previousVersionId = 1L;
		
		Mockito.when(mockMasterFileVersionRepository.updateIsLatest(Mockito.anyLong(), Mockito.anyLong())).thenReturn(1);
		
		Boolean result = masterFileVersionService.updateMasterFileVersionToLatest(masterFileId, previousVersionId);
		assertTrue(result);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).updateIsLatest(Mockito.anyLong(), Mockito.anyLong());
	}
	
	@Test
	public void testGetAllFiles_NullFilesReturned() {

		List<MasterFileVersionVO> masterFileVersions = null;
		Mockito.when(mockMasterFileVersionRepository.getAllFiles(Mockito.anyBoolean())).thenReturn(masterFileVersions );
		
		List<MasterFileVersionVO> returnedMasterFileVersions = masterFileVersionService.getAllFiles();
		assertNull(returnedMasterFileVersions);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).getAllFiles(Mockito.anyBoolean());
	}
	
	@Test
	public void testGetAllFiles_NoFilesReturned() {

		List<MasterFileVersionVO> masterFileVersions = new LinkedList<>();
		Mockito.when(mockMasterFileVersionRepository.getAllFiles(Mockito.anyBoolean())).thenReturn(masterFileVersions );
		
		List<MasterFileVersionVO> returnedMasterFileVersions = masterFileVersionService.getAllFiles();
		assertNotNull(returnedMasterFileVersions);
		assertTrue(returnedMasterFileVersions.size() == 0);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).getAllFiles(Mockito.anyBoolean());
	}
	
	@Test
	public void testGetAllFiles_1FileReturned() {

		List<MasterFileVersionVO> masterFileVersions = new LinkedList<>();
		masterFileVersions.add(getMasterFileVersionVO());
		Mockito.when(mockMasterFileVersionRepository.getAllFiles(Mockito.anyBoolean())).thenReturn(masterFileVersions );
		
		List<MasterFileVersionVO> returnedMasterFileVersions = masterFileVersionService.getAllFiles();
		assertNotNull(returnedMasterFileVersions);
		assertTrue(returnedMasterFileVersions.size() == 1);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).getAllFiles(Mockito.anyBoolean());
	}
	
	@Test
	public void testGetAllFiles_2FilesReturned() {

		List<MasterFileVersionVO> masterFileVersions = new LinkedList<>();
		masterFileVersions.add(getMasterFileVersionVO());
		masterFileVersions.add(getMasterFileVersionVO());
		Mockito.when(mockMasterFileVersionRepository.getAllFiles(Mockito.anyBoolean())).thenReturn(masterFileVersions );
		
		List<MasterFileVersionVO> returnedMasterFileVersions = masterFileVersionService.getAllFiles();
		assertNotNull(returnedMasterFileVersions);
		assertTrue(returnedMasterFileVersions.size() == 2);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).getAllFiles(Mockito.anyBoolean());
	}
	
	@Test
	public void testGetAllFileVersions_MasterFileIdIsNull() {
		Long masterFileId = null;
		
		List<MasterFileVersionVO> returnedMasterFileVersions = masterFileVersionService.getAllFileVersions(masterFileId);
		assertNull(returnedMasterFileVersions);
	}
	
	@Test
	public void testGetAllFileVersions_NullFilesReturned() {
		Long masterFileId = 1L;
		List<MasterFileVersionVO> masterFileVersions = null;
		Mockito.when(mockMasterFileVersionRepository.getAllFiles(Mockito.anyLong())).thenReturn(masterFileVersions );
		
		List<MasterFileVersionVO> returnedMasterFileVersions = masterFileVersionService.getAllFileVersions(masterFileId);
		assertNull(returnedMasterFileVersions);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).getAllFiles(Mockito.anyLong());
	}
	
	@Test
	public void testGetAllFileVersions_NoFilesReturned() {
		Long masterFileId = 1L;
		
		List<MasterFileVersionVO> masterFileVersions = new LinkedList<>();
		Mockito.when(mockMasterFileVersionRepository.getAllFiles(Mockito.anyLong())).thenReturn(masterFileVersions );
		
		List<MasterFileVersionVO> returnedMasterFileVersions = masterFileVersionService.getAllFileVersions(masterFileId);
		assertNotNull(returnedMasterFileVersions);
		assertTrue(returnedMasterFileVersions.size() == 0);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).getAllFiles(Mockito.anyLong());
	}
	
	@Test
	public void testGetAllFileVersions_1FileReturned() {
		Long masterFileId = 1L;
		
		List<MasterFileVersionVO> masterFileVersions = new LinkedList<>();
		masterFileVersions.add(getMasterFileVersionVO());
		Mockito.when(mockMasterFileVersionRepository.getAllFiles(Mockito.anyLong())).thenReturn(masterFileVersions );
		
		List<MasterFileVersionVO> returnedMasterFileVersions = masterFileVersionService.getAllFileVersions(masterFileId);
		assertNotNull(returnedMasterFileVersions);
		assertTrue(returnedMasterFileVersions.size() == 1);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).getAllFiles(Mockito.anyLong());
	}
	
	@Test
	public void testGetAllFileVersions_2FilesReturned() {
		Long masterFileId = 1L;
		
		List<MasterFileVersionVO> masterFileVersions = new LinkedList<>();
		masterFileVersions.add(getMasterFileVersionVO());
		masterFileVersions.add(getMasterFileVersionVO());
		Mockito.when(mockMasterFileVersionRepository.getAllFiles(Mockito.anyLong())).thenReturn(masterFileVersions );
		
		List<MasterFileVersionVO> returnedMasterFileVersions = masterFileVersionService.getAllFileVersions(masterFileId);
		assertNotNull(returnedMasterFileVersions);
		assertTrue(returnedMasterFileVersions.size() == 2);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).getAllFiles(Mockito.anyLong());
	}
	
	@Test
	public void testDeleteMasterFileVersion_MasterFileIdIsNull() {
		Long masterFileId = null;
		Long versionId = 1L;
		
		Boolean result = masterFileVersionService.deleteMasterFileVersion(masterFileId, versionId);
		assertFalse(result);
	}
	@Test
	public void testDeleteMasterFileVersion_VersionIdIsNull() {
		Long masterFileId = 1L;
		Long versionId = null;
		
		Boolean result = masterFileVersionService.deleteMasterFileVersion(masterFileId, versionId);
		assertFalse(result);
	}
	
	@Test
	public void testDeleteMasterFileVersion_FailedToDeleteMasterFileVersion() {
		Long masterFileId = 1L;
		Long versionId = 1L;
		
		Mockito.when(mockMasterFileVersionRepository.deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(0);
		
		Boolean result = masterFileVersionService.deleteMasterFileVersion(masterFileId, versionId);
		assertFalse(result);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong());
	}
	
	@Test
	public void testDeleteMasterFileVersion_Success() {
		Long masterFileId = 1L;
		Long versionId = 1L;
		
		Mockito.when(mockMasterFileVersionRepository.deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(1);
		
		Boolean result = masterFileVersionService.deleteMasterFileVersion(masterFileId, versionId);
		assertTrue(result);
		
		Mockito.verify(mockMasterFileVersionRepository, Mockito.times(1)).deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong());
	}
}
