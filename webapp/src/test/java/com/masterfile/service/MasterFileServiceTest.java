package com.masterfile.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataAccessException;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.masterfile.BaseTestClass;
import com.masterfile.domain.MasterFileVO;
import com.masterfile.repository.MasterFileRepository;
import com.masterfile.service.impl.MasterFileServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class MasterFileServiceTest extends BaseTestClass {

	@InjectMocks
	MasterFileServiceImpl masterFileService = new MasterFileServiceImpl();

	@Mock
	MasterFileRepository mockMasterFileRepository;

	@Mock
	DataAccessException mockDataAccessException;
	@After
	public void reset() {
		Mockito.reset(mockMasterFileRepository);
	}

	@Test
	public void testGetMasterFileVersion_MasterFileIdIsNull() {
		Long masterFileId = null;

		MasterFileVO returnedMasterFileVO = masterFileService.getMasterFile(masterFileId);
		assertNull(returnedMasterFileVO);
	}

	@Test
	public void testGetMasterFileVersion_NullMasterFileReturned() {
		Long masterFileId = 1L;

		Optional<MasterFileVO> masterFileVO = Optional.ofNullable(null);
		Mockito.when(mockMasterFileRepository.findById(Mockito.anyLong())).thenReturn(masterFileVO);

		MasterFileVO returnedMasterFileVO = masterFileService.getMasterFile(masterFileId);
		assertNull(returnedMasterFileVO);

		Mockito.verify(mockMasterFileRepository, Mockito.times(1)).findById(Mockito.anyLong());
	}

	@Test
	public void testGetMasterFileVersion_Success() {
		Long masterFileId = 1L;

		Optional<MasterFileVO> masterFileVO = Optional.of(getMasterFileVO());
		Mockito.when(mockMasterFileRepository.findById(Mockito.anyLong())).thenReturn(masterFileVO);

		MasterFileVO returnedMasterFileVO = masterFileService.getMasterFile(masterFileId);
		assertNotNull(returnedMasterFileVO);

		Mockito.verify(mockMasterFileRepository, Mockito.times(1)).findById(Mockito.anyLong());
	}

	@Test
	public void testSaveFile_MultipartFileIsNull() {

		MultipartFile multiPartFile = null;
		MasterFileVO returnedMasterFileVO = masterFileService.saveMasterFile(multiPartFile);
		assertNull(returnedMasterFileVO);
	}

	@Test
	public void testSaveFile_MultipartFileIsEmpty() {

		byte[] content = null;
		MultipartFile multiPartFile = new MockMultipartFile("name", content);
		MasterFileVO returnedMasterFileVO = masterFileService.saveMasterFile(multiPartFile);
		assertNull(returnedMasterFileVO);
	}

	@Test
	public void testSaveFile_FailedToSaveMasterFile() {

		byte[] content = "content".getBytes();
		MultipartFile multiPartFile = new MockMultipartFile("name", content);

		MasterFileVO masterFileVO = null;
		Mockito.when(mockMasterFileRepository.save(Mockito.isA(MasterFileVO.class))).thenReturn(masterFileVO);

		MasterFileVO returnedMasterFileVO = masterFileService.saveMasterFile(multiPartFile);
		assertNull(returnedMasterFileVO);

		Mockito.verify(mockMasterFileRepository, Mockito.times(1)).save(Mockito.isA(MasterFileVO.class));
	}
	
	@Test
	public void testSaveFile_Success() {

		byte[] content = "content".getBytes();
		MultipartFile multiPartFile = new MockMultipartFile("name", content);

		MasterFileVO masterFileVO = getMasterFileVO();
		Mockito.when(mockMasterFileRepository.save(Mockito.isA(MasterFileVO.class))).thenReturn(masterFileVO);

		MasterFileVO returnedMasterFileVO = masterFileService.saveMasterFile(multiPartFile);
		assertNotNull(returnedMasterFileVO);

		Mockito.verify(mockMasterFileRepository, Mockito.times(1)).save(Mockito.isA(MasterFileVO.class));
	}
	

	@Test
	public void testUpdateFile_MultipartFileIsNull() {

		MultipartFile multiPartFile = null;
		Long masterFileId = 1L;
		
		MasterFileVO returnedMasterFileVO = masterFileService.updateMasterFile(multiPartFile, masterFileId);
		assertNull(returnedMasterFileVO);
	}

	@Test
	public void testUpdateFile_MultipartFileIsEmpty() {

		byte[] content = null;
		Long masterFileId = 1L;
		
		MultipartFile multiPartFile = new MockMultipartFile("name", content);
		MasterFileVO returnedMasterFileVO = masterFileService.updateMasterFile(multiPartFile, masterFileId);
		assertNull(returnedMasterFileVO);
	}
	@Test
	public void testUpdateFile_MasterFileIdIsNull() {

		byte[] content = "content".getBytes();
		Long masterFileId = null;
		
		MultipartFile multiPartFile = new MockMultipartFile("name", content);

		MasterFileVO returnedMasterFileVO = masterFileService.updateMasterFile(multiPartFile, masterFileId);
		assertNull(returnedMasterFileVO);
	}
	@Test
	public void testUpdateFile_FailedToGetMasterFile() {

		byte[] content = "content".getBytes();
		Long masterFileId = 1L;
		
		MultipartFile multiPartFile = new MockMultipartFile("name", content);

		Optional<MasterFileVO> masterFileVO = Optional.ofNullable(null);
		Mockito.when(mockMasterFileRepository.findById(Mockito.anyLong())).thenReturn(masterFileVO);

		MasterFileVO returnedMasterFileVO = masterFileService.updateMasterFile(multiPartFile, masterFileId);
		assertNull(returnedMasterFileVO);

		Mockito.verify(mockMasterFileRepository, Mockito.times(1)).findById(Mockito.anyLong());
	}
	@Test
	public void testUpdateFile_FailedToSaveMasterFile() {

		byte[] content = "content".getBytes();
		Long masterFileId = 1L;
		
		MultipartFile multiPartFile = new MockMultipartFile("name", content);

		Optional<MasterFileVO> masterFileVO = Optional.of(getMasterFileVO());
		MasterFileVO savedMasterFileVO = null;
		Mockito.when(mockMasterFileRepository.findById(Mockito.anyLong())).thenReturn(masterFileVO);
		Mockito.when(mockMasterFileRepository.save(Mockito.isA(MasterFileVO.class))).thenReturn(savedMasterFileVO);
		
		MasterFileVO returnedMasterFileVO = masterFileService.updateMasterFile(multiPartFile, masterFileId);
		assertNull(returnedMasterFileVO);

		Mockito.verify(mockMasterFileRepository, Mockito.times(1)).findById(Mockito.anyLong());
		Mockito.verify(mockMasterFileRepository, Mockito.times(1)).save(Mockito.isA(MasterFileVO.class));
	}
	
	@Test
	public void testUpdateFile_Success() {

		byte[] content = "content".getBytes();
		Long masterFileId = 1L;
		
		MultipartFile multiPartFile = new MockMultipartFile("name", content);

		Optional<MasterFileVO> masterFileVO = Optional.of(getMasterFileVO());
		MasterFileVO savedMasterFileVO = getMasterFileVO();
		Mockito.when(mockMasterFileRepository.findById(Mockito.anyLong())).thenReturn(masterFileVO);
		Mockito.when(mockMasterFileRepository.save(Mockito.isA(MasterFileVO.class))).thenReturn(savedMasterFileVO);
		
		MasterFileVO returnedMasterFileVO = masterFileService.updateMasterFile(multiPartFile, masterFileId);
		assertNotNull(returnedMasterFileVO);

		Mockito.verify(mockMasterFileRepository, Mockito.times(1)).findById(Mockito.anyLong());
		Mockito.verify(mockMasterFileRepository, Mockito.times(1)).save(Mockito.isA(MasterFileVO.class));
	}
	
	@Test
	public void testDeleteMasterFile_MasterFileIdIsNull() {
		Long masterFileId = null;
		Boolean result = masterFileService.deleteMasterFile(masterFileId);
		assertFalse(result);
	}
	
	@Test
	public void testDeleteMasterFile_FailedToDeleteById() {
		Long masterFileId = 1L;
		Mockito.doThrow(mockDataAccessException).when(mockMasterFileRepository).deleteById(Mockito.anyLong());
		
		Boolean result = masterFileService.deleteMasterFile(masterFileId);
		assertFalse(result);
		
		Mockito.verify(mockMasterFileRepository, Mockito.times(1)).deleteById(Mockito.anyLong());
	}
	
	@Test
	public void testDeleteMasterFile_Success() {
		Long masterFileId = 1L;
		Mockito.doNothing().when(mockMasterFileRepository).deleteById(Mockito.anyLong());
		
		Boolean result = masterFileService.deleteMasterFile(masterFileId);
		assertTrue(result);
		
		Mockito.verify(mockMasterFileRepository, Mockito.times(1)).deleteById(Mockito.anyLong());
	}
}
