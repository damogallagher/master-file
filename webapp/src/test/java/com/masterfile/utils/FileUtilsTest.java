package com.masterfile.utils;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;

public class FileUtilsTest {

	@Test
	public void testConstructor() {
		FileUtils fileUtils = new FileUtils();

		assertNotNull(fileUtils);
	}
	
	
	@Test
	public void testGetFileName_MultiPartFileIsNull() {
		MockMultipartFile multiPartFile = null;
		
		String fileName = FileUtils.getFileName(multiPartFile);
		assertNull(fileName);
	}
	
	@Test
	public void testGetFileName_MultiPartFileIsEmpty() {
		String name = "Test File";
		byte[] content = null;
		MockMultipartFile multiPartFile = new MockMultipartFile(name, content);
		
		String fileName = FileUtils.getFileName(multiPartFile);
		assertNull(fileName);
	}

	
	@Test
	public void testGetFileName_Success() {
		String name = "name";
		String originalFilename = name;
		byte[] content = "content".getBytes();
		MockMultipartFile multiPartFile = new MockMultipartFile(name, originalFilename, "text", content);
		
		String fileName = FileUtils.getFileName(multiPartFile);
		assertNotNull(fileName);
		assertTrue(fileName.length() > 0);
	}
	
	@Test
	public void testGetFileType_MultiPartFileIsNull() {
		MockMultipartFile multiPartFile = null;
		
		String fileType = FileUtils.getFileType(multiPartFile);
		assertNull(fileType);
	}
	
	@Test
	public void testGetFileType_MultiPartFileIsEmpty() {
		String name = "Test File";
		byte[] content = null;
		MockMultipartFile multiPartFile = new MockMultipartFile(name, content);
		
		String fileType = FileUtils.getFileType(multiPartFile);
		assertNull(fileType);
	}
	@Test
	public void testGetFileType_FileNameIsNull() {
		String name = "name";
		String originalFilename = null;
		byte[] content = "content".getBytes();
		MockMultipartFile multiPartFile = new MockMultipartFile(name, originalFilename, "text", content);
		
		String fileType = FileUtils.getFileType(multiPartFile);
		assertNull(fileType);
	}
	@Test
	public void testGetFileType_FileNameIsEmpty() {
		String name = "name";
		String originalFilename = "";
		byte[] content = "content".getBytes();
		MockMultipartFile multiPartFile = new MockMultipartFile(name, originalFilename, "text", content);
		
		String fileType = FileUtils.getFileType(multiPartFile);
		assertNull(fileType);
	}
	@Test
	public void testGetFileType_Success() {
		String name = "name";
		String originalFilename = name;
		byte[] content = "content".getBytes();
		MockMultipartFile multiPartFile = new MockMultipartFile(name, originalFilename, "text", content);
		
		String fileType = FileUtils.getFileType(multiPartFile);
		assertNotNull(fileType);
		assertTrue(fileType.length() > 0);
	}
	@Test
	public void testGetFileContent_MultiPartFileIsNull() {
		MockMultipartFile multiPartFile = null;
		
		byte[] fileContent = FileUtils.getFileContent(multiPartFile);
		assertNull(fileContent);
	}
	@Test
	public void testGetFileContent_MultiPartFileContentIsNull() {
		String name = "name";
		String originalFilename = name;
		byte[] content = null;
		MockMultipartFile multiPartFile = new MockMultipartFile(name, originalFilename, "text", content);
		
		byte[] fileContent = FileUtils.getFileContent(multiPartFile);
		assertNull(fileContent);
	}
	@Test
	public void testGetFileContent_MultiPartFileContentIsEmpty() {
		String name = "name";
		String originalFilename = name;
		byte[] content = "".getBytes();
		MockMultipartFile multiPartFile = new MockMultipartFile(name, originalFilename, "text", content);
		
		byte[] fileContent = FileUtils.getFileContent(multiPartFile);
		assertNull(fileContent);
	}
	@Test
	public void testGetFileContent_Success() {
		String name = "name";
		String originalFilename = name;
		byte[] content = "content".getBytes();
		MockMultipartFile multiPartFile = new MockMultipartFile(name, originalFilename, "text", content);
		
		byte[] fileContent = FileUtils.getFileContent(multiPartFile);
		assertNotNull(fileContent);
		assertTrue(fileContent.length > 0);
	}
	@Test
	public void testGetFileSize_MulipartFileIsNull() {
		MockMultipartFile multiPartFile = null;
		
		Long fileSize = FileUtils.getFileSize(multiPartFile);
		assertNull(fileSize);
	}
	@Test
	public void testGetFileSize_MulipartFileContentIsNull() {
		String name = "name";
		String originalFilename = name;
		byte[] content = null;
		MockMultipartFile multiPartFile = new MockMultipartFile(name, originalFilename, "text", content);
		
		Long fileSize = FileUtils.getFileSize(multiPartFile);
		assertNull(fileSize);
	}
	@Test
	public void testGetFileSize_MulipartFileContentIsEmpty() {
		String name = "name";
		String originalFilename = name;
		byte[] content = "".getBytes();
		MockMultipartFile multiPartFile = new MockMultipartFile(name, originalFilename, "text", content);
		
		Long fileSize = FileUtils.getFileSize(multiPartFile);
		assertNull(fileSize);
	}
	@Test
	public void testGetFileSize_Success() {
		String name = "name";
		String originalFilename = name;
		byte[] content = "content".getBytes();
		MockMultipartFile multiPartFile = new MockMultipartFile(name, originalFilename, "text", content);
		
		Long fileSize = FileUtils.getFileSize(multiPartFile);
		assertNotNull(fileSize);
		assertTrue(fileSize > 0);
	}
}
