package com.masterfile.rest;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.masterfile.BaseTestClass;
import com.masterfile.MasterFileApplication;
import com.masterfile.config.BeanConfig;
import com.masterfile.domain.MasterFileVO;
import com.masterfile.domain.MasterFileVersionVO;
import com.masterfile.repository.MasterFileRepository;
import com.masterfile.repository.MasterFileVersionRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = {MasterFileApplication.class, BeanConfig.class})
@AutoConfigureMockMvc 
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@AutoConfigureTestDatabase
public class MasterFileRestControllerIntegrationTest extends BaseTestClass {

	@Autowired
    private MockMvc mockMvc;
 
    @Autowired
    private MasterFileRepository masterFileRepository;
    @Autowired
    private MasterFileVersionRepository masterFileVersionRepository;
    
	private MasterFileVersionVO createFileVersion() {
		MasterFileVO masterFileVO = getMasterFileVO();
		masterFileRepository.save(masterFileVO);
		
		MasterFileVersionVO masterFileVersionVO = getMasterFileVersionVO();
		masterFileVersionVO.setMasterFile(masterFileVO);
		masterFileVersionRepository.save(masterFileVersionVO);
		return masterFileVersionVO;
	}
    
    
    @Test
    public void testSaveMasterFile()
      throws Exception {
     
    	MockMultipartFile jsonFile = new MockMultipartFile("file", "info.json", "application/json", "{\"json\": \"someValue\"}".getBytes());

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.multipart("/masterfile")
                        .file(jsonFile))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isOk())
        				.andExpect(jsonPath("$").exists())
                        .andExpect(jsonPath("$.id").exists())
                        .andExpect(jsonPath("$.versionId").value(1))
                        .andExpect(jsonPath("$.isLatestVersion").value(true))
                        .andExpect(jsonPath("$.fileName").exists())
                        .andExpect(jsonPath("$.fileSize").exists())
                        .andExpect(jsonPath("$.fileType").exists())
                        .andExpect(jsonPath("$.fileContent").doesNotExist())
                        .andExpect(jsonPath("$.dateAdded").exists())
                        .andExpect(jsonPath("$.dateUpdated").exists())
                        .andExpect(jsonPath("$.masterFile.id").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileName").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileType").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileSize").exists())
                        .andExpect(jsonPath("$.masterFile.dateAdded").exists())
                        .andExpect(jsonPath("$.masterFile.dateUpdated").exists())
                        .andReturn();
    }
    
    @Test
    public void testUpdateMasterFile()
      throws Exception {
     
    	MasterFileVersionVO masterFileVersion = createFileVersion();
    	
    	
    	MockMultipartFile jsonFile = new MockMultipartFile("file", "info.json", "application/json", "{\"json\": \"someValue\"}".getBytes());

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.multipart("/masterfile/{masterFileId}", masterFileVersion.getMasterFile().getId())
                        .file(jsonFile))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isOk())
                        .andExpect(jsonPath("$").exists())
                        .andExpect(jsonPath("$.id").exists())
                        .andExpect(jsonPath("$.versionId").value(2))
                        .andExpect(jsonPath("$.isLatestVersion").value(true))
                        .andExpect(jsonPath("$.fileName").exists())
                        .andExpect(jsonPath("$.fileSize").exists())
                        .andExpect(jsonPath("$.fileType").exists())
                        .andExpect(jsonPath("$.fileContent").doesNotExist())
                        .andExpect(jsonPath("$.dateAdded").exists())
                        .andExpect(jsonPath("$.dateUpdated").exists())
                        .andExpect(jsonPath("$.masterFile.id").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileName").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileType").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileSize").exists())
                        .andExpect(jsonPath("$.masterFile.dateAdded").exists())
                        .andExpect(jsonPath("$.masterFile.dateUpdated").exists())
                        .andReturn();

    }


    @Test
    public void testGetAllFiles()
      throws Exception {
     
    	MasterFileVersionVO masterFileVersion = createFileVersion();
    	assertNotNull(masterFileVersion);
    	
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/masterfile"))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isOk())
        				.andExpect(jsonPath("$").exists())
                        .andExpect(jsonPath("$[0].id").exists())
                        .andExpect(jsonPath("$[0].versionId").value(2))
                        .andExpect(jsonPath("$[0].isLatestVersion").value(true))
                        .andExpect(jsonPath("$[0].fileName").exists())
                        .andExpect(jsonPath("$[0].fileSize").exists())
                        .andExpect(jsonPath("$[0].fileType").exists())
                        .andExpect(jsonPath("$[0].fileContent").doesNotExist())
                        .andExpect(jsonPath("$[0].dateAdded").exists())
                        .andExpect(jsonPath("$[0].dateUpdated").exists())
                        .andExpect(jsonPath("$[0].masterFile.id").exists())
                        .andExpect(jsonPath("$[0].masterFile.originalFileName").exists())
                        .andExpect(jsonPath("$[0].masterFile.originalFileType").exists())
                        .andExpect(jsonPath("$[0].masterFile.originalFileSize").exists())
                        .andExpect(jsonPath("$[0].masterFile.dateAdded").exists())
                        .andExpect(jsonPath("$[0].masterFile.dateUpdated").exists())
                        .andReturn();

    }
    
    @Test
    public void testGetLatestFileVersion()
      throws Exception {
     
    	MasterFileVersionVO masterFileVersion = createFileVersion();
    	assertNotNull(masterFileVersion);
    	
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/masterfile/{masterFileId}", masterFileVersion.getMasterFile().getId()))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isOk())
        				.andExpect(jsonPath("$").exists())
                        .andExpect(jsonPath("$.id").exists())
                        .andExpect(jsonPath("$.versionId").value(1))
                        .andExpect(jsonPath("$.isLatestVersion").value(true))
                        .andExpect(jsonPath("$.fileName").exists())
                        .andExpect(jsonPath("$.fileSize").exists())
                        .andExpect(jsonPath("$.fileType").exists())
                        .andExpect(jsonPath("$.fileContent").doesNotExist())
                        .andExpect(jsonPath("$.dateAdded").exists())
                        .andExpect(jsonPath("$.dateUpdated").exists())
                        .andExpect(jsonPath("$.masterFile.id").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileName").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileType").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileSize").exists())
                        .andExpect(jsonPath("$.masterFile.dateAdded").exists())
                        .andExpect(jsonPath("$.masterFile.dateUpdated").exists())
                        .andReturn();

    }
    
    @Test
    public void testGetLatestFileVersionWithVersionId()
      throws Exception {
     
    	MasterFileVersionVO masterFileVersion = createFileVersion();
    	assertNotNull(masterFileVersion);
    	
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/masterfile/{masterFileId}/{versionId}", masterFileVersion.getMasterFile().getId(), masterFileVersion.getVersionId()))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isOk())
        				.andExpect(jsonPath("$").exists())
                        .andExpect(jsonPath("$.id").exists())
                        .andExpect(jsonPath("$.versionId").value(1))
                        .andExpect(jsonPath("$.isLatestVersion").value(true))
                        .andExpect(jsonPath("$.fileName").exists())
                        .andExpect(jsonPath("$.fileSize").exists())
                        .andExpect(jsonPath("$.fileType").exists())
                        .andExpect(jsonPath("$.fileContent").doesNotExist())
                        .andExpect(jsonPath("$.dateAdded").exists())
                        .andExpect(jsonPath("$.dateUpdated").exists())
                        .andExpect(jsonPath("$.masterFile.id").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileName").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileType").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileSize").exists())
                        .andExpect(jsonPath("$.masterFile.dateAdded").exists())
                        .andExpect(jsonPath("$.masterFile.dateUpdated").exists())
                        .andReturn();

    }
    
    @Test
    public void testGetAllFileVersions()
      throws Exception {
     
    	MasterFileVersionVO masterFileVersion = createFileVersion();
    	assertNotNull(masterFileVersion);
    	
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/masterfile/versions/{masterFileId}", masterFileVersion.getMasterFile().getId()))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isOk())
        				.andExpect(jsonPath("$").exists())
                        .andExpect(jsonPath("$[0].id").exists())
                        .andExpect(jsonPath("$[0].versionId").exists())
                        .andExpect(jsonPath("$[0].isLatestVersion").value(true))
                        .andExpect(jsonPath("$[0].fileName").exists())
                        .andExpect(jsonPath("$[0].fileSize").exists())
                        .andExpect(jsonPath("$[0].fileType").exists())
                        .andExpect(jsonPath("$[0].fileContent").doesNotExist())
                        .andExpect(jsonPath("$[0].dateAdded").exists())
                        .andExpect(jsonPath("$[0].dateUpdated").exists())
                        .andExpect(jsonPath("$[0].masterFile.id").exists())
                        .andExpect(jsonPath("$[0].masterFile.originalFileName").exists())
                        .andExpect(jsonPath("$[0].masterFile.originalFileType").exists())
                        .andExpect(jsonPath("$[0].masterFile.originalFileSize").exists())
                        .andExpect(jsonPath("$[0].masterFile.dateAdded").exists())
                        .andExpect(jsonPath("$[0].masterFile.dateUpdated").exists())
                        .andReturn();

    }
    
    @Test
    public void testDeleteMasterFile()
      throws Exception {
     
    	MasterFileVersionVO masterFileVersion = createFileVersion();
    	assertNotNull(masterFileVersion);
    	
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete("/masterfile/{masterFileId}", masterFileVersion.getMasterFile().getId()))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isOk())
        				.andExpect(content().string("true"))
                        .andReturn();

    }
    
    @Test
    public void testDeleteMasterFileWithVersionId()
      throws Exception {
     
    	MasterFileVersionVO masterFileVersion = createFileVersion();
    	assertNotNull(masterFileVersion);
    	
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete("/masterfile/{masterFileId}/{versionId}", masterFileVersion.getMasterFile().getId(), masterFileVersion.getVersionId()))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isOk())
        				.andExpect(content().string("true"))
                        .andReturn();

    }
}
