package com.masterfile.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.multipart.MultipartFile;

import com.masterfile.BaseTestClass;
import com.masterfile.domain.MasterFileVO;
import com.masterfile.domain.MasterFileVersionVO;
import com.masterfile.service.IFileService;

@RunWith(MockitoJUnitRunner.class)
public class MasterFileRestControllerUnitTest extends BaseTestClass {

	@InjectMocks
	MasterFileRestController masterFileRestController = new MasterFileRestController();
	
	@Autowired
    private MockMvc mockMvc;
 
	@Mock
    private IFileService mockFileService;
	@Mock
    private ExecutorService executorService;
    
	private MasterFileVO masterFile;
	private MasterFileVersionVO masterFileVersion;
	private List<MasterFileVersionVO> masterFileVersionList;
	@Before
	public void setUp() {
		
		this.mockMvc = MockMvcBuilders.standaloneSetup(masterFileRestController).build();
		
		ExecutorService executorService = Executors.newFixedThreadPool(20);
		masterFileRestController.setExecutorService(executorService);
		
		masterFile        = getMasterFileVO();
		masterFile.setId(1L);
		
		masterFileVersion = getMasterFileVersionVO();
		masterFileVersion.setId(1L);
		masterFileVersion.setMasterFile(masterFile);
		
		masterFileVersionList = new LinkedList<>();
		masterFileVersionList.add(masterFileVersion);
	}

    @After
    public void after() {
    	Mockito.reset(mockFileService);
    }
    
    @Test
    public void testSaveMasterFile_FailedToSaveFile() throws Exception{
    	
    	Mockito.when(mockFileService.saveFile(Mockito.isA(MultipartFile.class))).thenReturn(null);
    	MockMultipartFile jsonFile = new MockMultipartFile("file", "info.json", "application/json", "{\"json\": \"someValue\"}".getBytes());

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.multipart("/masterfile")
                        .file(jsonFile))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isInternalServerError())
        				.andExpect(jsonPath("$").exists())
                        .andReturn();
        
        Mockito.verify(mockFileService, Mockito.times(1)).saveFile(Mockito.isA(MultipartFile.class));
    }
    
    @Test
    public void testSaveMasterFile_Success() throws Exception{
    	
    	Mockito.when(mockFileService.saveFile(Mockito.isA(MultipartFile.class))).thenReturn(masterFileVersion);
    	MockMultipartFile jsonFile = new MockMultipartFile("file", "info.json", "application/json", "{\"json\": \"someValue\"}".getBytes());

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.multipart("/masterfile")
                        .file(jsonFile))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isOk())
        				.andExpect(jsonPath("$").exists())
                        .andExpect(jsonPath("$.id").exists())
                        .andExpect(jsonPath("$.versionId").value(1))
                        .andExpect(jsonPath("$.isLatestVersion").value(true))
                        .andExpect(jsonPath("$.fileName").exists())
                        .andExpect(jsonPath("$.fileSize").exists())
                        .andExpect(jsonPath("$.fileType").exists())
                        .andExpect(jsonPath("$.fileContent").doesNotExist())
                        .andExpect(jsonPath("$.dateAdded").exists())
                        .andExpect(jsonPath("$.dateUpdated").exists())
                        .andExpect(jsonPath("$.masterFile.id").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileName").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileType").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileSize").exists())
                        .andExpect(jsonPath("$.masterFile.dateAdded").exists())
                        .andExpect(jsonPath("$.masterFile.dateUpdated").exists())
                        .andReturn();
        
        Mockito.verify(mockFileService, Mockito.times(1)).saveFile(Mockito.isA(MultipartFile.class));
    }
    @Test
    public void testUpdateMasterFile_FailedToUpdateFile() throws Exception {
    	Mockito.when(mockFileService.updateFile(Mockito.isA(MultipartFile.class), Mockito.anyLong())).thenReturn(null);
    	MockMultipartFile jsonFile = new MockMultipartFile("file", "info.json", "application/json", "{\"json\": \"someValue\"}".getBytes());

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.multipart("/masterfile/{masterFileId}", masterFileVersion.getMasterFile().getId())
                        .file(jsonFile))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isInternalServerError())
                        .andReturn();

        Mockito.verify(mockFileService, Mockito.times(1)).updateFile(Mockito.isA(MultipartFile.class), Mockito.anyLong());
    }
    
   
    @Test
    public void testUpdateMasterFile_Success() throws Exception {
    	Mockito.when(mockFileService.updateFile(Mockito.isA(MultipartFile.class), Mockito.anyLong())).thenReturn(masterFileVersion);
    	MockMultipartFile jsonFile = new MockMultipartFile("file", "info.json", "application/json", "{\"json\": \"someValue\"}".getBytes());

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.multipart("/masterfile/{masterFileId}", masterFileVersion.getMasterFile().getId())
                        .file(jsonFile))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isOk())
                        .andExpect(jsonPath("$").exists())
                        .andExpect(jsonPath("$.id").exists())
                        .andExpect(jsonPath("$.versionId").exists())
                        .andExpect(jsonPath("$.isLatestVersion").value(true))
                        .andExpect(jsonPath("$.fileName").exists())
                        .andExpect(jsonPath("$.fileSize").exists())
                        .andExpect(jsonPath("$.fileType").exists())
                        .andExpect(jsonPath("$.fileContent").doesNotExist())
                        .andExpect(jsonPath("$.dateAdded").exists())
                        .andExpect(jsonPath("$.dateUpdated").exists())
                        .andExpect(jsonPath("$.masterFile.id").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileName").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileType").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileSize").exists())
                        .andExpect(jsonPath("$.masterFile.dateAdded").exists())
                        .andExpect(jsonPath("$.masterFile.dateUpdated").exists())
                        .andReturn();

        Mockito.verify(mockFileService, Mockito.times(1)).updateFile(Mockito.isA(MultipartFile.class), Mockito.anyLong());
    }

    @Test
    public void testGetAllFiles_ReturnedListIsEmpty() throws Exception {
    	masterFileVersionList = new LinkedList<>();
    	Mockito.when(mockFileService.getAllFiles()).thenReturn(masterFileVersionList);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/masterfile"))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isInternalServerError())
                        .andReturn();
        
        Mockito.verify(mockFileService, Mockito.times(1)).getAllFiles();
    }
    @Test
    public void testGetAllFiles_ReturnedListIsNull() throws Exception {
    	masterFileVersionList = null;
    	Mockito.when(mockFileService.getAllFiles()).thenReturn(masterFileVersionList);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/masterfile"))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isInternalServerError())
                        .andReturn();
        
        Mockito.verify(mockFileService, Mockito.times(1)).getAllFiles();
    }
    @Test
    public void testGetAllFiles_Success() throws Exception {
    	Mockito.when(mockFileService.getAllFiles()).thenReturn(masterFileVersionList);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/masterfile"))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isOk())
        				.andExpect(jsonPath("$").exists())
                        .andExpect(jsonPath("$[0].id").exists())
                        .andExpect(jsonPath("$[0].versionId").exists())
                        .andExpect(jsonPath("$[0].isLatestVersion").value(true))
                        .andExpect(jsonPath("$[0].fileName").exists())
                        .andExpect(jsonPath("$[0].fileSize").exists())
                        .andExpect(jsonPath("$[0].fileType").exists())
                        .andExpect(jsonPath("$[0].fileContent").doesNotExist())
                        .andExpect(jsonPath("$[0].dateAdded").exists())
                        .andExpect(jsonPath("$[0].dateUpdated").exists())
                        .andExpect(jsonPath("$[0].masterFile.id").exists())
                        .andExpect(jsonPath("$[0].masterFile.originalFileName").exists())
                        .andExpect(jsonPath("$[0].masterFile.originalFileType").exists())
                        .andExpect(jsonPath("$[0].masterFile.originalFileSize").exists())
                        .andExpect(jsonPath("$[0].masterFile.dateAdded").exists())
                        .andExpect(jsonPath("$[0].masterFile.dateUpdated").exists())
                        .andReturn();
        
        Mockito.verify(mockFileService, Mockito.times(1)).getAllFiles();
    }
    @Test
    public void testGetLatestFileVersion_FailedToGetLatestVersion() throws Exception {
    	Mockito.when(mockFileService.getLatestFileVersion(Mockito.anyLong())).thenReturn(null);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/masterfile/{masterFileId}", masterFileVersion.getMasterFile().getId()))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isInternalServerError())
                        .andReturn();

        Mockito.verify(mockFileService, Mockito.times(1)).getLatestFileVersion(Mockito.anyLong());
    }

    @Test
    public void testGetLatestFileVersion_Success() throws Exception {
    	Mockito.when(mockFileService.getLatestFileVersion(Mockito.anyLong())).thenReturn(masterFileVersion);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/masterfile/{masterFileId}", masterFileVersion.getMasterFile().getId()))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isOk())
        				.andExpect(jsonPath("$").exists())
                        .andExpect(jsonPath("$.id").exists())
                        .andExpect(jsonPath("$.versionId").value(1))
                        .andExpect(jsonPath("$.isLatestVersion").value(true))
                        .andExpect(jsonPath("$.fileName").exists())
                        .andExpect(jsonPath("$.fileSize").exists())
                        .andExpect(jsonPath("$.fileType").exists())
                        .andExpect(jsonPath("$.fileContent").doesNotExist())
                        .andExpect(jsonPath("$.dateAdded").exists())
                        .andExpect(jsonPath("$.dateUpdated").exists())
                        .andExpect(jsonPath("$.masterFile.id").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileName").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileType").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileSize").exists())
                        .andExpect(jsonPath("$.masterFile.dateAdded").exists())
                        .andExpect(jsonPath("$.masterFile.dateUpdated").exists())
                        .andReturn();

        Mockito.verify(mockFileService, Mockito.times(1)).getLatestFileVersion(Mockito.anyLong());
    }
    @Test
    public void testGetLatestFileVersionWithVersionId_Failed() throws Exception {
    	Mockito.when(mockFileService.getFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(null);
    	
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/masterfile/{masterFileId}/{versionId}", masterFileVersion.getMasterFile().getId(), masterFileVersion.getVersionId()))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isInternalServerError())
                        .andReturn();
        Mockito.verify(mockFileService, Mockito.times(1)).getFileVersion(Mockito.anyLong(), Mockito.anyLong());
    }
    @Test
    public void testGetLatestFileVersionWithVersionId_Success() throws Exception {
    	Mockito.when(mockFileService.getFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(masterFileVersion);
    	
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/masterfile/{masterFileId}/{versionId}", masterFileVersion.getMasterFile().getId(), masterFileVersion.getVersionId()))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isOk())
        				.andExpect(jsonPath("$").exists())
                        .andExpect(jsonPath("$.id").exists())
                        .andExpect(jsonPath("$.versionId").value(1))
                        .andExpect(jsonPath("$.isLatestVersion").value(true))
                        .andExpect(jsonPath("$.fileName").exists())
                        .andExpect(jsonPath("$.fileSize").exists())
                        .andExpect(jsonPath("$.fileType").exists())
                        .andExpect(jsonPath("$.fileContent").doesNotExist())
                        .andExpect(jsonPath("$.dateAdded").exists())
                        .andExpect(jsonPath("$.dateUpdated").exists())
                        .andExpect(jsonPath("$.masterFile.id").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileName").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileType").exists())
                        .andExpect(jsonPath("$.masterFile.originalFileSize").exists())
                        .andExpect(jsonPath("$.masterFile.dateAdded").exists())
                        .andExpect(jsonPath("$.masterFile.dateUpdated").exists())
                        .andReturn();
        Mockito.verify(mockFileService, Mockito.times(1)).getFileVersion(Mockito.anyLong(), Mockito.anyLong());
    }
    @Test
    public void testGetAllFileVersions_ReturnedListIsEmpty() throws Exception {
    	masterFileVersionList = new LinkedList<>();
    	Mockito.when(mockFileService.getAllFileVersions(Mockito.anyLong())).thenReturn(masterFileVersionList);
    	
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/masterfile/versions/{masterFileId}", masterFileVersion.getMasterFile().getId()))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isInternalServerError())
                        .andReturn();

        Mockito.verify(mockFileService, Mockito.times(1)).getAllFileVersions(Mockito.anyLong());
    }
    @Test
    public void testGetAllFileVersions_ReturnedListIsNull() throws Exception {
    	masterFileVersionList = null;
    	Mockito.when(mockFileService.getAllFileVersions(Mockito.anyLong())).thenReturn(masterFileVersionList);
    	
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/masterfile/versions/{masterFileId}", masterFileVersion.getMasterFile().getId()))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isInternalServerError())
                        .andReturn();

        Mockito.verify(mockFileService, Mockito.times(1)).getAllFileVersions(Mockito.anyLong());
    }
    @Test
    public void testGetAllFileVersions_Success() throws Exception {
    	Mockito.when(mockFileService.getAllFileVersions(Mockito.anyLong())).thenReturn(masterFileVersionList);
    	
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/masterfile/versions/{masterFileId}", masterFileVersion.getMasterFile().getId()))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isOk())
        				.andExpect(jsonPath("$").exists())
                        .andExpect(jsonPath("$[0].id").exists())
                        .andExpect(jsonPath("$[0].versionId").exists())
                        .andExpect(jsonPath("$[0].isLatestVersion").value(true))
                        .andExpect(jsonPath("$[0].fileName").exists())
                        .andExpect(jsonPath("$[0].fileSize").exists())
                        .andExpect(jsonPath("$[0].fileType").exists())
                        .andExpect(jsonPath("$[0].fileContent").doesNotExist())
                        .andExpect(jsonPath("$[0].dateAdded").exists())
                        .andExpect(jsonPath("$[0].dateUpdated").exists())
                        .andExpect(jsonPath("$[0].masterFile.id").exists())
                        .andExpect(jsonPath("$[0].masterFile.originalFileName").exists())
                        .andExpect(jsonPath("$[0].masterFile.originalFileType").exists())
                        .andExpect(jsonPath("$[0].masterFile.originalFileSize").exists())
                        .andExpect(jsonPath("$[0].masterFile.dateAdded").exists())
                        .andExpect(jsonPath("$[0].masterFile.dateUpdated").exists())
                        .andReturn();

        Mockito.verify(mockFileService, Mockito.times(1)).getAllFileVersions(Mockito.anyLong());
    }
    
    @Test
    public void testDeleteMasterFile_FailedToDelete() throws Exception {     
    	Mockito.when(mockFileService.deleteMasterFile(Mockito.anyLong())).thenReturn(false);
    	
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete("/masterfile/{masterFileId}", masterFileVersion.getMasterFile().getId()))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isInternalServerError())
                        .andReturn();
        
        Mockito.verify(mockFileService, Mockito.times(1)).deleteMasterFile(Mockito.anyLong());
    }
    @Test
    public void testDeleteMasterFile_Success() throws Exception {     
    	Mockito.when(mockFileService.deleteMasterFile(Mockito.anyLong())).thenReturn(true);
    	
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete("/masterfile/{masterFileId}", masterFileVersion.getMasterFile().getId()))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isOk())
        				.andExpect(content().string("true"))
                        .andReturn();
        
        Mockito.verify(mockFileService, Mockito.times(1)).deleteMasterFile(Mockito.anyLong());
    }
    @Test
    public void testDeleteMasterFileWithVersionId_FailedToDelete() throws Exception {
    	Mockito.when(mockFileService.deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(false);
    	
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete("/masterfile/{masterFileId}/{versionId}", masterFileVersion.getMasterFile().getId(), masterFileVersion.getVersionId()))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isInternalServerError())
                        .andReturn();

        Mockito.verify(mockFileService, Mockito.times(1)).deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong());
    }
    @Test
    public void testDeleteMasterFileWithVersionId_Success() throws Exception {
    	Mockito.when(mockFileService.deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong())).thenReturn(true);
    	
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete("/masterfile/{masterFileId}/{versionId}", masterFileVersion.getMasterFile().getId(), masterFileVersion.getVersionId()))
        		        .andDo(print())
        		        .andDo(log())                        
                        .andReturn();
    	
        mockMvc.perform(asyncDispatch(mvcResult))
        				.andDo(print())
        				.andDo(log())
        				.andExpect(status().isOk())
        				.andExpect(content().string("true"))
                        .andReturn();

        Mockito.verify(mockFileService, Mockito.times(1)).deleteMasterFileVersion(Mockito.anyLong(), Mockito.anyLong());
    }
}
