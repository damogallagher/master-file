package com.masterfile.config;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(MockitoJUnitRunner.class)
public class BeanConfigTest {

	@InjectMocks
	BeanConfig beanConfig = new BeanConfig();
	
	
	@Before
	public void setUp() {
		ReflectionTestUtils.setField(beanConfig, "executorServiceNumberOfThreads", 10);
	}
	
	@Test
	public void testExecutorService() {
		assertNotNull(beanConfig.executorService());
	}
	@Test
	public void testModele() {
		assertNotNull(beanConfig.module());
	}
}
