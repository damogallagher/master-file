package com.masterfile.config;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SwaggerConfigTest {

	@InjectMocks
	SwaggerConfig swaggerConfig = new SwaggerConfig();
	

	@Test
	public void testApi() {
		assertNotNull(swaggerConfig.api());
	}
}
