package com.masterfile;

import java.time.LocalDateTime;

import org.junit.Ignore;

import com.masterfile.domain.MasterFileVO;
import com.masterfile.domain.MasterFileVersionVO;

@Ignore
public abstract class BaseTestClass {

	
	public MasterFileVO getMasterFileVO() {
		MasterFileVO masterFile = new MasterFileVO();
		masterFile.setDateAdded(LocalDateTime.now());
		masterFile.setDateUpdated(LocalDateTime.now());
		masterFile.setOriginalFileName("file.pdf");
		masterFile.setOriginalFileSize(1024L);
		masterFile.setOriginalFileType("pdf");	
		return masterFile;
	}
	
	public MasterFileVersionVO getMasterFileVersionVO() {
		MasterFileVersionVO masterFileVersion = new MasterFileVersionVO();
		masterFileVersion.setDateAdded(LocalDateTime.now());
		masterFileVersion.setDateUpdated(LocalDateTime.now());
		masterFileVersion.setFileName("file.pdf");
		masterFileVersion.setFileSize(1024L);
		masterFileVersion.setFileType("pdf");
		masterFileVersion.setFileContent("6368616e67654c6f6773".getBytes());
		masterFileVersion.setIsLatestVersion(true);
		masterFileVersion.setVersionId(1L);
		return masterFileVersion;
	}
}
