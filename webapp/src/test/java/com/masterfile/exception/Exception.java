package com.masterfile.exception;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class Exception {

	@Test
	public void testMessage() {
		MasterFileException masterFileException = new MasterFileException("exception");
		assertNotNull(masterFileException);
	}
	
	@Test
	public void testThrowable() {
		MasterFileException masterFileException = new MasterFileException(new NullPointerException());
		assertNotNull(masterFileException);
	}
	
	@Test
	public void testMessageAndThrowable() {
		MasterFileException masterFileException = new MasterFileException("exception",new NullPointerException());
		assertNotNull(masterFileException);
	}
}
