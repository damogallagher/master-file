package com.masterfile.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.masterfile.domain.MasterFileVO;
import com.masterfile.domain.MasterFileVersionVO;

public interface IMasterFileVersionService {

	/**
	 * Method to get the master file version based on the masterFileId
	 * @param masterFileId
	 * @return
	 */
	MasterFileVersionVO getMasterFileVersion(Long masterFileId);
	
	/**
	 * Method to get a master file version based on masterFileId and versionId
	 * @param masterFileId
	 * @param versionId
	 * @return
	 */
	MasterFileVersionVO getMasterFileVersion(Long masterFileId, Long versionId);
	
	/**
	 * Method to save a file version
	 * @param multiPartFile
	 * @param masterFile 
	 * @return
	 */
	MasterFileVersionVO saveMasterFileVersion(MultipartFile multiPartFile, MasterFileVO masterFile);
	
	/**
	 * Method to save a masterFile version
	 * @param masterFileVersion
	 * @return
	 */
	MasterFileVersionVO saveMasterFileVersion(MasterFileVersionVO masterFileVersion);
	
	/**
	 * Method to update a master file version to the database
	 * @param multiPartFile
	 * @param masterFile 
	 * @return
	 */
	MasterFileVersionVO updateMasterFileVersion(MultipartFile multiPartFile, MasterFileVO masterFile);

	/**
	 * Method to update a master file to be the latest version
	 * @param masterFileId
	 * @param previousVersionId
	 * @return
	 */
	Boolean updateMasterFileVersionToLatest(Long masterFileId, Long previousVersionId);
	
	/**
	 * Method to get all master file versions
	 * @return
	 */
	List<MasterFileVersionVO> getAllFiles();

	/**
	 * Method to get all file versions
	 * @param masterFileId
	 * @return
	 */
	List<MasterFileVersionVO> getAllFileVersions(Long masterFileId);

	/**
	 * Method to delete a master file for an id and version
	 * @param masterFileId
	 * @param versionId
	 * @return
	 */
	Boolean deleteMasterFileVersion(Long masterFileId, Long versionId);

}
