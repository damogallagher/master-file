package com.masterfile.service;

import org.springframework.web.multipart.MultipartFile;

import com.masterfile.domain.MasterFileVO;

public interface IMasterFileService {

	/**
	 * Method to get a master file object based on id
	 * @param masterFileId
	 * @return
	 */
	MasterFileVO getMasterFile(Long masterFileId);
	
	/**
	 * Method to save a masterFile
	 * @param multiPartFile
	 * @return
	 */
	MasterFileVO saveMasterFile(MultipartFile multiPartFile);

	/**
	 * Method to update a master file 
	 * @param multiPartFile 
	 * @param masterFileId 
	 * @return
	 */
	MasterFileVO updateMasterFile(MultipartFile multiPartFile, Long masterFileId);

	/**
	 * Method to delete a master file
	 * @param masterFileId
	 * @return
	 */
	Boolean deleteMasterFile(Long masterFileId);
}
