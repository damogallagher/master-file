package com.masterfile.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.masterfile.domain.MasterFileVO;
import com.masterfile.domain.MasterFileVersionVO;
import com.masterfile.repository.MasterFileVersionRepository;
import com.masterfile.service.IMasterFileVersionService;
import com.masterfile.utils.FileUtils;

@Service
public class MasterFileVersionServiceImpl implements IMasterFileVersionService {

	// Static class logger
    private static final Logger LOGGER = LoggerFactory.getLogger(MasterFileVersionServiceImpl.class);
    
    @Autowired
    private MasterFileVersionRepository masterFileVersionRepository;
	
    /**
	 * Method to get the master file version based on the masterFileId
	 * @param masterFileId
	 * @return
	 */
    @Override
	public MasterFileVersionVO getMasterFileVersion(Long masterFileId) {
		LOGGER.debug("Entered getMasterFileVersion - masterFileId:{}", masterFileId);
		
		MasterFileVersionVO masterFileVersion = null;
		if (masterFileId == null) {
			LOGGER.error("masterFileId passed in is null ");
			return masterFileVersion;
		}
		
		masterFileVersion = masterFileVersionRepository.findByMasterFileIdAndLatest(masterFileId, true);  
		
		LOGGER.debug("Exiting getMasterFile");
		return masterFileVersion;
	}
    
	/**
	 * Method to get a master file version based on masterFileId and versionId
	 * @param masterFileId
	 * @param versionId
	 * @return
	 */
	public MasterFileVersionVO getMasterFileVersion(Long masterFileId, Long versionId) {
		LOGGER.debug("Entered getFileVersion - masterFileId:{}, versionId:{}", masterFileId, versionId);
		
		MasterFileVersionVO masterFileVersion = null;
		if (masterFileId == null || versionId == null) {
			LOGGER.error("masterFileId or versionId passed in is null");
			return masterFileVersion;
		}
		
		masterFileVersion = masterFileVersionRepository.findByMasterFileIdAndVersion(masterFileId, versionId);	
		
		LOGGER.debug("Exiting getFileVersion");
		return masterFileVersion;
	}
    
	/**
	 * Method to save a file version
	 * @param multiPartFile
	 * @param masterFile
	 * @return
	 */
    @Override
	public MasterFileVersionVO saveMasterFileVersion(MultipartFile multiPartFile, MasterFileVO masterFile) {
        LOGGER.debug("Entered saveMasterFileVersion");
		
        MasterFileVersionVO savedMasterFileVersion = saveFileVersion(multiPartFile, masterFile, 1L);
		
		LOGGER.debug("Exiting saveMasterFileVersion");
		return savedMasterFileVersion;
	}
	
	/**
	 * Method to save a file version to the database
	 * @param multiPartFile
	 * @param masterFile
	 * @param versionId
	 * @return
	 */
	private MasterFileVersionVO saveFileVersion(MultipartFile multiPartFile, MasterFileVO masterFile, Long versionId) {
		LOGGER.debug("Entered saveFileVersion - versionId:{}", versionId);
		MasterFileVersionVO savedMasterFileVersion = null;
		if (multiPartFile == null || multiPartFile.isEmpty() || masterFile == null) {
			LOGGER.error("multiPartFile passed in is null or empty or the masterFile");
			return savedMasterFileVersion;
		}
		
		String fileName    = FileUtils.getFileName(multiPartFile);
		String fileType    = FileUtils.getFileType(multiPartFile);
		byte[] fileContent = FileUtils.getFileContent(multiPartFile);
		Long fileSize      = FileUtils.getFileSize(multiPartFile);
		savedMasterFileVersion = new MasterFileVersionVO();
		savedMasterFileVersion.setFileName(fileName);
		savedMasterFileVersion.setFileType(fileType);		
		savedMasterFileVersion.setFileContent(fileContent);
		savedMasterFileVersion.setFileSize(fileSize);
		savedMasterFileVersion.setIsLatestVersion(true);
		savedMasterFileVersion.setVersionId(versionId);

		savedMasterFileVersion.setMasterFile(masterFile);
		savedMasterFileVersion = saveMasterFileVersion(savedMasterFileVersion);
		
		LOGGER.debug("Exiting saveFileVersion");
		return savedMasterFileVersion;
	}
	
	/**
	 * Method to save a masterFile version
	 * @param masterFileVersion
	 * @return
	 */
	@Override
	public MasterFileVersionVO saveMasterFileVersion(MasterFileVersionVO masterFileVersion) {
		LOGGER.debug("Entered saveMasterFileVersion");
		MasterFileVersionVO savedMasterFileVersion = null;
		if (masterFileVersion == null) {
			LOGGER.error("masterFileVersion passed in is null or empty");
			return savedMasterFileVersion;
		}
		
		savedMasterFileVersion = masterFileVersionRepository.save(masterFileVersion);

		LOGGER.debug("Exiting saveMasterFileVersion");
		return savedMasterFileVersion;
	}
	
	/**
	 * Method to update a file version to the database
	 * @param multiPartFile
	 * @param masterFile 
	 * @return
	 */
	@Override
	public MasterFileVersionVO updateMasterFileVersion(MultipartFile multiPartFile, MasterFileVO masterFile) {
		LOGGER.debug("Entered updateMasterFileVersion");
		
		MasterFileVersionVO updatedMasterFileVersion = null;
		if (multiPartFile == null || multiPartFile.isEmpty() || masterFile == null) {
			LOGGER.error("multiPartFile passed in is null or empty or the masterFile is null");
			return updatedMasterFileVersion;
		}
		Long masterFileId = masterFile.getId();
		MasterFileVersionVO oldMasterFileVersion = getMasterFileVersion(masterFileId);
		if (oldMasterFileVersion == null) {
			LOGGER.error("No masterFileVersion exists for the masterFileId:{}", masterFileId);
			return updatedMasterFileVersion;
		}

		int rowsUpdated = masterFileVersionRepository.updateIsNotLatest(masterFileId);
		LOGGER.info("rowsUpdated:{}", rowsUpdated);
		
		if (rowsUpdated <= 0) {
			LOGGER.error("Failed to update the old file version to not be the latest");
			return updatedMasterFileVersion;
		}
		Long newVersionId = oldMasterFileVersion.getVersionId() + 1L;
		LOGGER.info("newVersionId:{}", newVersionId);
		
		updatedMasterFileVersion = saveFileVersion(multiPartFile, masterFile, newVersionId);
		
		LOGGER.debug("Exiting updateMasterFileVersion");
		return updatedMasterFileVersion;
	}

	/**
	 * Method to update a master file to be the latest version
	 * @param masterFileId
	 * @param previousVersionId
	 * @return
	 */
	@Override
	public Boolean updateMasterFileVersionToLatest(Long masterFileId, Long previousVersionId) {
       LOGGER.debug("Entered updateMasterFileVersionToLatest");
		
       Boolean result = false;
		if (masterFileId == null || previousVersionId == null) {
			LOGGER.error("masterFileId or previousVersionId passed in is null");
			return result;
		}
		
		int rowsUpdated = masterFileVersionRepository.updateIsLatest(masterFileId, previousVersionId);
		LOGGER.info("rowsUpdated:{}", rowsUpdated);
		
		if (rowsUpdated <= 0) {
			LOGGER.error("Failed to update the old file version to be the latest for the masterFileId:{} and previousVersionId:{}", masterFileId, previousVersionId);
			return result;
		}
		result = true;
		
		LOGGER.debug("Exiting updateMasterFileVersionToLatest - result:{}", result);
		return result;
	}

	/**
	 * Method to get all master file versions
	 * @return
	 */
	public List<MasterFileVersionVO> getAllFiles() {
		LOGGER.debug("Entered getAllFiles");
		
		List<MasterFileVersionVO> masterFileVersions = masterFileVersionRepository.getAllFiles(true);		
		
		LOGGER.debug("Exiting getAllFiles");
		return masterFileVersions;
	}

	/**
	 * Method to get all file versions
	 * @param masterFileId
	 * @return
	 */
	public List<MasterFileVersionVO> getAllFileVersions(Long masterFileId) {
		LOGGER.debug("Entered getAllFileVersions - masterFileId:{}", masterFileId);
		List<MasterFileVersionVO> masterFileVersionList = null;
		if (masterFileId == null) {
			LOGGER.error("masterFileVersion passed in is null or empty");
			return masterFileVersionList;
		}
		
		masterFileVersionList = masterFileVersionRepository.getAllFiles(masterFileId);

		LOGGER.debug("Exiting getAllFileVersions");
		return masterFileVersionList;
	}
	
	/**
	 * Method to delete a master file for an id and version
	 * @param masterFileId
	 * @param versionId
	 * @return
	 */
	public Boolean deleteMasterFileVersion(Long masterFileId, Long versionId) {
		LOGGER.debug("Entered deleteMasterFileVersion - masterFileId:{}, versionId:{}", masterFileId, versionId);
		Boolean result = false;
		if (masterFileId == null || versionId == null) {
			LOGGER.error("masterFileVersion passed in is null or empty");
			return result;
		}		
		
		int rowsDeleted = masterFileVersionRepository.deleteMasterFileVersion(masterFileId, versionId);
		LOGGER.debug("rowsDeleted:{}", rowsDeleted);
		if (rowsDeleted > 0) {
			result = true;
		}
		
		LOGGER.debug("Exiting deleteMasterFileVersion - result:{}", result);
		return result;
	}
}
