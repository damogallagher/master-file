package com.masterfile.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.masterfile.domain.MasterFileVO;
import com.masterfile.repository.MasterFileRepository;
import com.masterfile.service.IMasterFileService;
import com.masterfile.utils.FileUtils;


//https://github.com/springframeworkguru/spring-boot-mysql-example/blob/master/src/main/java/guru/springframework/services/ProductServiceImpl.java
@Service
public class MasterFileServiceImpl implements IMasterFileService {

	// Static class logger
    private static final Logger LOGGER = LoggerFactory.getLogger(MasterFileServiceImpl.class);
    
    @Autowired
    private MasterFileRepository masterFileRepository;
    
	/**
	 * Method to get a master file object based on id
	 * @param masterFileId
	 * @return
	 */
    @Override
	public MasterFileVO getMasterFile(Long masterFileId) {
		LOGGER.debug("Entered getMasterFile - masterFileId:{}", masterFileId);
		
		MasterFileVO masterFile = null;
		if (masterFileId == null) {
			LOGGER.error("masterFileId passed in is null");
			return masterFile;
		}
		
		masterFile = masterFileRepository.findById(masterFileId).orElse(null);	    
		
		LOGGER.debug("Exiting getMasterFile");
		return masterFile;
	}
	
	/**
	 * Method to save a masterFile
	 * @param multiPartFile
	 * @return
	 */
	@Override
	public MasterFileVO saveMasterFile(MultipartFile multiPartFile) {
		LOGGER.debug("Entered saveMasterFile");
		MasterFileVO savedMasterFile = null;
		if (multiPartFile == null || multiPartFile.isEmpty()) {
			LOGGER.error("multiPartFile passed in is null or empty");
			return savedMasterFile;
		}
		
		String fileName = FileUtils.getFileName(multiPartFile);
		String fileType = FileUtils.getFileType(multiPartFile);
		Long fileSize   = FileUtils.getFileSize(multiPartFile);
		
		MasterFileVO masterFile = new MasterFileVO();
		masterFile.setOriginalFileName(fileName);
		masterFile.setOriginalFileType(fileType);
		masterFile.setOriginalFileSize(fileSize);
		savedMasterFile = masterFileRepository.save(masterFile);

		LOGGER.debug("Exiting saveMasterFile");
		return savedMasterFile;
	}
	
	/**
	 * Method to save a masterFile
	 * @param masterFile
	 * @return
	 */
	private MasterFileVO saveMasterFile(MasterFileVO masterFile) {
		LOGGER.debug("Entered saveMasterFile");
		MasterFileVO savedMasterFile = null;

		masterFile.setDateUpdated(null);
		
		savedMasterFile = masterFileRepository.save(masterFile);

		LOGGER.debug("Exiting saveMasterFile");
		return savedMasterFile;
	}
	
	/**
	 * Method to update a master file to the database
	 * @param multiPartFile
	 * @param masterFileId 
	 * @return
	 */
	@Override
	public MasterFileVO updateMasterFile(MultipartFile multiPartFile, Long masterFileId) {
		LOGGER.debug("Entered updateMasterFile - masterFileId:{}", masterFileId);
		
		MasterFileVO updatedMasterFile = null;
		if (multiPartFile == null || multiPartFile.isEmpty() || masterFileId == null) {
			LOGGER.error("multiPartFile is null or empty or masterFileId is null");
			return updatedMasterFile;
		}
		
		MasterFileVO masterFile = getMasterFile(masterFileId);
		if (masterFile == null) {
			LOGGER.error("No masterFile exists for the masterFileId:{}", masterFileId);
			return updatedMasterFile;
		}
		updatedMasterFile = saveMasterFile(masterFile);
		
		LOGGER.debug("Exiting updateMasterFile");
		return updatedMasterFile;
	}
	

	/**
	 * Method to delete a master file
	 * @param masterFileId
	 * @return
	 */
	public Boolean deleteMasterFile(Long masterFileId) {
		LOGGER.debug("Entered deleteMasterFile - masterFileId:{}", masterFileId);
		
		Boolean result = false;
		if (masterFileId == null) {
			LOGGER.error("masterFileId passed in is null");
			return result;
		}
		
		try {
			masterFileRepository.deleteById(masterFileId);
			result = true;
		} catch (DataAccessException e) {
			LOGGER.error("A DataAccessException has occured deleting the master file. Exception:{}", e);
			result = false;
		}

		LOGGER.debug("Exiting deleteMasterFile - result:{}", result);
		return result;
	}

}
