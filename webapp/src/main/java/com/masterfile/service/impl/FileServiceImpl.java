package com.masterfile.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import com.masterfile.domain.MasterFileVO;
import com.masterfile.domain.MasterFileVersionVO;
import com.masterfile.exception.MasterFileException;
import com.masterfile.service.IFileService;
import com.masterfile.service.IMasterFileService;
import com.masterfile.service.IMasterFileVersionService;

@Service
public class FileServiceImpl implements IFileService {

	// Static class logger
	private static final Logger LOGGER = LoggerFactory.getLogger(FileServiceImpl.class);

	
	@Autowired
	private IMasterFileService masterFileService;
	
	@Autowired
	private IMasterFileVersionService masterFileVersionService;
	
	/**
	 * Method to save a file
	 * @param multiPartFile
	 * @return
	 */
	@Transactional
	public MasterFileVersionVO saveFile(MultipartFile multiPartFile) {
		LOGGER.debug("Entered saveFile");
		MasterFileVersionVO masterFileVersion = null;
		if (multiPartFile == null || multiPartFile.isEmpty()) {
			LOGGER.error("multiPartFile passed in is null or empty ");
			return masterFileVersion;
		}		
		
		MasterFileVO savedMasterFile = masterFileService.saveMasterFile(multiPartFile);
		if (savedMasterFile == null) {
			LOGGER.error("Failed to save new file to the database");
			throw new MasterFileException("Failed to save new file to the database");
		}

		masterFileVersion = masterFileVersionService.saveMasterFileVersion(multiPartFile, savedMasterFile);
		if (masterFileVersion == null) {
			LOGGER.error("Failed to save new file version to the database");
			throw new MasterFileException("Failed to save new file version to the database");
		}
		
		LOGGER.debug("Exiting saveFile");
		return masterFileVersion;
	}

	/**
	 * Method to update a file
	 * @param multiPartFile
	 * @param masterFileId
	 * @return
	 */
	@Transactional
	public MasterFileVersionVO updateFile(MultipartFile multiPartFile, Long masterFileId) {
		LOGGER.debug("Entered updateFile - masterFileId:{}", masterFileId);
		
		MasterFileVersionVO updatedMasterFileVersion = null;
		if (multiPartFile == null || multiPartFile.isEmpty() || masterFileId == null) {
			LOGGER.error("multiPartFile passed in is null or empty or the masterFileId is null");
			return updatedMasterFileVersion;
		}
		
		MasterFileVO updatedMasterFile = masterFileService.updateMasterFile(multiPartFile, masterFileId);
		if (updatedMasterFile == null) {
			LOGGER.error("Failed to update file with masterFileId:{}", masterFileId);
			throw new MasterFileException("Failed to update file with masterFileId:" + masterFileId);
		}
		
		updatedMasterFileVersion = masterFileVersionService.updateMasterFileVersion(multiPartFile, updatedMasterFile);
		if (updatedMasterFileVersion == null) {
			LOGGER.error("Failed to update master file version with masterFileId:{}", masterFileId);
			throw new MasterFileException("Failed to update master file version with masterFileId:" + masterFileId);
		}
		
		LOGGER.debug("Exiting updateFile");
		return updatedMasterFileVersion;
	}
	
	
	/**
	 * Method to get all latest file versions
	 * @return
	 */
	public List<MasterFileVersionVO> getAllFiles() {
		LOGGER.debug("Entered getAllFiles");
		
		List<MasterFileVersionVO> masterFileVersions = masterFileVersionService.getAllFiles();		
		
		LOGGER.debug("Exiting getAllFiles");
		return masterFileVersions;
	}
	
	/**
	 * Method to get the latest version of a file base on masterFileId
	 * @param masterFileId
	 * @return
	 */
	public MasterFileVersionVO getLatestFileVersion(Long masterFileId) {
		LOGGER.debug("Entered getLatestFileVersion - masterFileId:{}", masterFileId);
		
		MasterFileVersionVO masterFileVersion = null;
		if (masterFileId == null) {
			LOGGER.error("masterFileId passed in is null");
			return masterFileVersion;
		}
		
		masterFileVersion = masterFileVersionService.getMasterFileVersion(masterFileId);	
		
		LOGGER.debug("Exiting getLatestFileVersion");
		return masterFileVersion;	
	}
	

	/**
	 * Method to get a version of a file based on masterFileId and versionId
	 * @param masterFileId
	 * @param versionId
	 * @return
	 */
	public MasterFileVersionVO getFileVersion(Long masterFileId, Long versionId) {
		LOGGER.debug("Entered getFileVersion - masterFileId:{}, versionId:{}", masterFileId, versionId);
		
		MasterFileVersionVO masterFileVersion = null;
		if (masterFileId == null || versionId == null) {
			LOGGER.error("masterFileId or versionId passed in is null");
			return masterFileVersion;
		}
		
		masterFileVersion = masterFileVersionService.getMasterFileVersion(masterFileId, versionId);	
		
		LOGGER.debug("Exiting getFileVersion");
		return masterFileVersion;
	}
	
	/**
	 * Method to get all file versions
	 * @param masterFileId
	 * @return
	 */
	public List<MasterFileVersionVO> getAllFileVersions(Long masterFileId) {
        LOGGER.debug("Entered getAllFileVersions - masterFileId:{}", masterFileId);
		
        List<MasterFileVersionVO> masterFileVersions = null;
		if (masterFileId == null) {
			LOGGER.error("masterFileId passed in is null");
			return masterFileVersions;
		}
		
		masterFileVersions = masterFileVersionService.getAllFileVersions(masterFileId);		
		
		LOGGER.debug("Exiting getAllFileVersions ");
		return masterFileVersions;
	}
	
	/**
	 * Method to delete a master file
	 * @param masterFileId
	 * @return
	 */	
	public Boolean deleteMasterFile(Long masterFileId) {
		LOGGER.debug("Entered deleteMasterFile - masterFileId:{}", masterFileId);
		
		Boolean result = false;
		if (masterFileId == null) {
			LOGGER.error("masterFileId passed in is null");
			return result;
		}
		
		result = masterFileService.deleteMasterFile(masterFileId);		
		
		LOGGER.debug("Exiting deleteMasterFile - result:{}", result);
		return result;
	}
	
	/**
	 * Method to delete a master file version for an id and version
	 * @param masterFileId
	 * @param versionId
	 * @return
	 */
	@Transactional
	public Boolean deleteMasterFileVersion(Long masterFileId, Long versionId) {
		LOGGER.debug("Entered deleteMasterFileVersion - masterFileId:{}, versionId:{}", masterFileId, versionId);
		
		Boolean result = false;
		if (masterFileId == null || versionId == null) {
			LOGGER.error("masterFileId or versionId passed in is null");
			return result;
		}
		
		MasterFileVersionVO masterFileVersion = masterFileVersionService.getMasterFileVersion(masterFileId, versionId);
		if (masterFileVersion == null) {
			LOGGER.error("Failed to get the masterFileVersion for the masterFileId:{} and versionId:{}", masterFileId, versionId);
			return result;
		}
		
		boolean isFileToDeleteTheLatest = masterFileVersion.getIsLatestVersion();
		LOGGER.debug("isFileToDeleteTheLatest:{}", isFileToDeleteTheLatest);
		
		//Delete the current version
		result = masterFileVersionService.deleteMasterFileVersion(masterFileId, versionId);		
		if (!result) {
			LOGGER.error("Failed to delete master file version for the masterFileId:{} and versionId:{}", masterFileId, versionId);
			throw new MasterFileException("Failed to delete master file version for the masterFileId:"+masterFileId+" and versionId:"+ versionId);
		}
		
		if (isFileToDeleteTheLatest) {
			result = setPreviousFileToBeLatest(masterFileId);
			if (!result) {
				LOGGER.error("Failed to set preious file to be the latest for the masterFileId:{} and versionId:{}", masterFileId, versionId);
				throw new MasterFileException("Failed to set preious file to be the latest for the masterFileId:"+masterFileId+" and versionId:"+ versionId);
			}
		}
		

		LOGGER.debug("Exiting deleteMasterFileVersion - result:{}", result);
		return result;
	}

	/**
	 * Method to set a previous version of a file to be the latest
	 * 
	 * @param masterFileId
	 */
	private Boolean setPreviousFileToBeLatest(Long masterFileId) {
		LOGGER.debug("Entered setPreviousFileToBeLatest - masterFileId:{}", masterFileId);
		Boolean result = false;
		// Get the previous versions of the file so we can set 1 to be the latest
		List<MasterFileVersionVO> masterFileVersionList = masterFileVersionService.getAllFileVersions(masterFileId);

		// If no previous versions exist - delete the master file
		// Else, update a previous version to be the latest version now
		if (CollectionUtils.isEmpty(masterFileVersionList)) {
			LOGGER.error("No previous versions exist for the masterFileId:{}. Delete this master file", masterFileId);
			result = masterFileService.deleteMasterFile(masterFileId);
		} else {
			MasterFileVersionVO newMasterFileVersion = masterFileVersionList.get(0);
			Long previousVersionId = newMasterFileVersion.getVersionId();
			result = masterFileVersionService.updateMasterFileVersionToLatest(masterFileId, previousVersionId);
		}
		LOGGER.debug("Exiting setPreviousFileToBeLatest - result:{}", result);
		return result;
	}
}
