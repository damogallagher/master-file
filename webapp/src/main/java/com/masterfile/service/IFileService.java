package com.masterfile.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.masterfile.domain.MasterFileVersionVO;

public interface IFileService {

	/**
	 * Method to save a file
	 * @param multiPartFile
	 * @return
	 */
	MasterFileVersionVO saveFile(MultipartFile multiPartFile);

	/**
	 * Method to update a file
	 * @param multiPartFile
	 * @param masterFileId
	 * @return
	 */
	MasterFileVersionVO updateFile(MultipartFile multiPartFile, Long masterFileId);

	/**
	 * Method to get all latest file versions
	 * @return
	 */
	List<MasterFileVersionVO> getAllFiles();

	/**
	 * Method to get the latest version of a file base on masterFileId
	 * @param masterFileId
	 * @return
	 */
	MasterFileVersionVO getLatestFileVersion(Long masterFileId);

	/**
	 * Method to get a version of a file based on masterFileId and versionId
	 * @param masterFileId
	 * @param versionId
	 * @return
	 */
	MasterFileVersionVO getFileVersion(Long masterFileId, Long versionId);
	
	/**
	 * Method to get all file versions
	 * @param masterFileId
	 * @return
	 */
	List<MasterFileVersionVO> getAllFileVersions(Long masterFileId);

	/**
	 * Method to delete a master file
	 * @param masterFileId
	 * @return
	 */
	Boolean deleteMasterFile(Long masterFileId);
	
	/**
	 * Method to delete a master file version for an id and version
	 * @param masterFileId
	 * @param versionId
	 * @return
	 */
	Boolean deleteMasterFileVersion(Long masterFileId, Long versionId);

}
