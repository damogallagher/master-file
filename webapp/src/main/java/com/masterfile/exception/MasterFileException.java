package com.masterfile.exception;

public class MasterFileException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6484592531627592518L;

	/**
	 * Exception with string only
	 * @param exceptionMessage
	 */
	public MasterFileException(String exceptionMessage) 
    { 
        super(exceptionMessage); 
    } 
	
	/**
	 * Exception with string and throwable
	 * @param throwable
	 */
	public MasterFileException(Throwable throwable) 
    { 
        super(throwable); 
    } 
	
	/**
	 * Exception with string and throwable
	 * @param exceptionMessage
	 * @param throwable
	 */
	public MasterFileException(String exceptionMessage, Throwable throwable) 
    { 
        super(exceptionMessage, throwable); 
    } 
}
