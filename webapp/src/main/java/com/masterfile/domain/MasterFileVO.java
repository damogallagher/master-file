package com.masterfile.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name="T_MASTER_FILE")
public class MasterFileVO extends BaseDomainObject  implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 902558392277560862L;

	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
	private Long id;
	
	@Column(name = "original_file_name", nullable=false)
	@NotNull 
	@Size(min=2, max=255)
	private String originalFileName;
	
	@Column(name = "original_file_type", nullable=false)
	@NotNull 
	@Size(min=2, max=100)
	private String originalFileType;
	
	@Column(name = "original_file_size", nullable=false)
	@NotNull 
	private Long originalFileSize;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "masterFile")
	@Cascade(CascadeType.ALL)	
	private List<MasterFileVersionVO> masterFileVersions = new ArrayList<MasterFileVersionVO>(0);
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public List<MasterFileVersionVO> getMasterFileVersions() {
		return masterFileVersions;
	}

	public void setMasterFileVersions(List<MasterFileVersionVO> masterFileVersions) {
		this.masterFileVersions = masterFileVersions;
	}

	public String getOriginalFileType() {
		return originalFileType;
	}

	public void setOriginalFileType(String originalFileType) {
		this.originalFileType = originalFileType;
	}

	public Long getOriginalFileSize() {
		return originalFileSize;
	}

	public void setOriginalFileSize(Long originalFileSize) {
		this.originalFileSize = originalFileSize;
	}

	@Override
	public String toString() {
		return "MasterFile [id=" + id + ", originalFileName=" + originalFileName + ", originalFileType="
				+ originalFileType + ", originalFileSize=" + originalFileSize + "]";
	}	
	
}
