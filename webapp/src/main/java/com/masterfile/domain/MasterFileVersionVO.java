package com.masterfile.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="T_MASTER_FILE_VERSION")
public class MasterFileVersionVO extends BaseDomainObject implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 902558392277560862L;

	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
	private Long id;

	@Column(name = "version_id", nullable=false)
	@NotNull
	private Long versionId;

	@Column(name = "is_latest_version", nullable=false)
	@NotNull
	private Boolean isLatestVersion;
	
	@Column(name = "file_name", nullable=false)
	@NotNull 
	@Size(min=2, max=255)
	private String fileName;
	
	@Column(name = "file_type", nullable=false)
	@NotNull
	@Size(min=2, max=100)
	private String fileType;
	
	@Column(name = "file_size", nullable=false)
	@NotNull
	private Long fileSize;
	
	@Lob
	@JsonIgnore
	@Column(name = "file_content", nullable=false)
	@NotNull
    private byte[] fileContent;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "masterfile_id", nullable = false)	
	private MasterFileVO masterFile;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersionId() {
		return versionId;
	}

	public void setVersionId(Long versionId) {
		this.versionId = versionId;
	}
 
	public Boolean getIsLatestVersion() {
		return isLatestVersion;
	}

	public void setIsLatestVersion(Boolean isLatestVersion) {
		this.isLatestVersion = isLatestVersion;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public byte[] getFileContent() {
		return fileContent;
	}

	public void setFileContent(byte[] fileContent) {
		this.fileContent = fileContent;
	}	

	
	public MasterFileVO getMasterFile() {
		return masterFile;
	}

	public void setMasterFile(MasterFileVO masterFile) {
		this.masterFile = masterFile;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	@Override
	public String toString() {
		return "MasterFileVersion [id=" + id + ", versionId=" + versionId + ", isLatestVersion=" + isLatestVersion
				+ ", fileName=" + fileName + ", fileType=" + fileType + ", fileSize=" + fileSize + ", masterFile=" + masterFile + "]";
	}
	    
	
}
