package com.masterfile.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@MappedSuperclass
public abstract class BaseDomainObject {

	@Column(name = "date_added", nullable=false)
	private LocalDateTime dateAdded;
	
	@Column(name = "date_updated", nullable=false)
	private LocalDateTime dateUpdated;
	
	@PrePersist
    public void prePersist() {
		dateAdded = LocalDateTime.now();
		dateUpdated = LocalDateTime.now();
    }
 
    @PreUpdate
    public void preUpdate() {
    	dateUpdated = LocalDateTime.now();
    }

	public LocalDateTime getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(LocalDateTime dateAdded) {
		this.dateAdded = dateAdded;
	}

	public LocalDateTime getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(LocalDateTime dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	@Override
	public String toString() {
		return "BaseDomainObject [dateAdded=" + dateAdded + ", dateUpdated=" + dateUpdated + "]";
	}
    
    
}
