package com.masterfile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MasterFileApplication {

	/**
	 * Application Main Method
	 * @param args
	 */
	public static void main(String[] args) {
		//Set the default profile for spring to use - if not specified with the arg spring.profiles.active - then this will be used
		System.setProperty("spring.profiles.default", "local");
		SpringApplication.run(MasterFileApplication.class);
	}

}
