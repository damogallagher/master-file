package com.masterfile.repository;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.masterfile.domain.MasterFileVersionVO;

@Repository
public interface MasterFileVersionRepository extends CrudRepository<MasterFileVersionVO, Long> {

	/**
	 * Query to get the latest file for a master file id
	 * @param masterFileId
	 * @param isLatest
	 * @return
	 */
	@Query(value = "SELECT mfv FROM MasterFileVersionVO mfv WHERE mfv.masterFile.id=:masterFileId and mfv.isLatestVersion=:isLatest")
	MasterFileVersionVO findByMasterFileIdAndLatest(@Param("masterFileId") Long masterFileId, @Param("isLatest") boolean isLatest);

	/**
	 * Method to find a master file based on masterFileId and versionId
	 * @param masterFileId
	 * @param versionId
	 * @return
	 */
	@Query(value = "SELECT mfv FROM MasterFileVersionVO mfv WHERE mfv.masterFile.id=:masterFileId and mfv.versionId=:versionId")
	MasterFileVersionVO findByMasterFileIdAndVersion(@Param("masterFileId") Long masterFileId, @Param("versionId") Long versionId);
	
	/**
	 * Method to set the older version of the file to not be the latest
	 * @param masterFileId
	 * @return
	 */
	@Modifying
	@Query(value = "UPDATE MasterFileVersionVO mfv set mfv.isLatestVersion=false WHERE mfv.masterFile.id=:masterFileId and mfv.isLatestVersion=true")
	int updateIsNotLatest(@Param("masterFileId") Long masterFileId);
	
	/**
	 * Method to set a master file version to be the latests
	 * @param masterFileId
	 * @return
	 */
	@Modifying
	@Query(value = "UPDATE MasterFileVersionVO mfv set mfv.isLatestVersion=true WHERE mfv.masterFile.id=:masterFileId and mfv.versionId=:versionId")
	int updateIsLatest(@Param("masterFileId") Long masterFileId, @Param("versionId") Long versionId);

	/**
	 * Method to return all latest files
	 * @param b
	 * @return
	 */
	@Query(value = "SELECT mfv FROM MasterFileVersionVO mfv WHERE mfv.isLatestVersion=:isLatest")
	List<MasterFileVersionVO> getAllFiles(@Param("isLatest") boolean isLatest);

	/**
	 * Method to get all files including those that are not latest
	 * @param masterFileId
	 * @return
	 */
	@Query(value = "SELECT mfv FROM MasterFileVersionVO mfv WHERE mfv.masterFile.id=:masterFileId ORDER BY mfv.versionId DESC")
	List<MasterFileVersionVO> getAllFiles(@Param("masterFileId")  Long masterFileId);

	/**
	 * Method to delete a master file version for an id and versionId
	 * @param masterFileId
	 * @param versionId
	 * @return
	 */
	@Modifying
	@Query(value = "DELETE FROM MasterFileVersionVO mfv WHERE mfv.masterFile.id=:masterFileId and mfv.versionId=:versionId")
	int deleteMasterFileVersion(@Param("masterFileId")  Long masterFileId, @Param("versionId")  Long versionId);


}
