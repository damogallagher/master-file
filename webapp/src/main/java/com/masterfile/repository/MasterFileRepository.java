package com.masterfile.repository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.masterfile.domain.MasterFileVO;

@Repository
public interface MasterFileRepository extends CrudRepository<MasterFileVO, Long> {


	
}
