package com.masterfile.config;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;

@Configuration
public class BeanConfig {   
	
	@Value("${executor.service.numberOfThreads}")
	private int executorServiceNumberOfThreads;
	/**
	 * Bean for the executor service to use
	 * @return
	 */
	@Bean
	public ExecutorService executorService() {            
	    return Executors.newFixedThreadPool(executorServiceNumberOfThreads);
	}
	
	/**
	 * Bean for managing lazy loaded dependencies with Hibernate
	 * @return
	 */
	@Bean
	protected Module module() {
	    return new Hibernate5Module();
	}
	
}