package com.masterfile.utils;

import java.io.IOException;

import javax.activation.MimetypesFileTypeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

public class FileUtils {

	// Static class logger
	private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);

	/**
	 * Protected class constructor
	 */
	protected FileUtils() {
		
	}
	
	/**
	 * Method to get a multipart file name
	 * @param multiPartFile
	 * @return
	 */
	public static String getFileName(MultipartFile multiPartFile) {
		LOGGER.debug("Entered getFileName");
		String fileName = null;
		if (multiPartFile == null || multiPartFile.isEmpty()) {
			LOGGER.error("multiPartFile passed in is null or empty");
			return fileName;
		}

		fileName = multiPartFile.getOriginalFilename();
		
		LOGGER.debug("Exiting getFileName - fileName:{}", fileName);
		return fileName;
	}

	/**
	 * Method to get the multiPartFile type
	 * @param multiPartFile
	 * @return
	 */
	public static String getFileType(MultipartFile multiPartFile) {
		LOGGER.debug("Entered getFileType");
		String fileType = null;
		String fileName = getFileName(multiPartFile);
		if (StringUtils.isEmpty(fileName)) {
			LOGGER.error("Failed to get file type for the file passed in");
			return fileType;
		}
		
		MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
	    fileType = fileTypeMap.getContentType(fileName);	    
		
		LOGGER.debug("Exiting getFileType - fileType:{}", fileType);
		return fileType;
	}

	/**
	 * Method to get the file content
	 * @param multiPartFile
	 * @return
	 */
	public static byte[] getFileContent(MultipartFile multiPartFile) {
		LOGGER.debug("Entered getFileContent");
		byte[] fileContent = null;
		if (multiPartFile == null || multiPartFile.isEmpty()) {
			LOGGER.error("multiPartFile passed in is null or empty");
			return fileContent;
		}
		
		try {
			fileContent = multiPartFile.getBytes();
		} catch (IOException e) {
			LOGGER.error("An IOException occured getting the file content");
			fileContent = null;
		}
		
		LOGGER.debug("Exiting getFileContent");
		return fileContent;
	}

	/**
	 * Method to get a multipart file size
	 * @param multiPartFile
	 * @return
	 */
	public static Long getFileSize(MultipartFile multiPartFile) {
		LOGGER.debug("Entered getFileSize");
		Long fileSize = null;
		if (multiPartFile == null || multiPartFile.isEmpty()) {
			LOGGER.error("multiPartFile passed in is null or empty");
			return fileSize;
		}
		
		fileSize = multiPartFile.getSize();

		LOGGER.debug("Exiting getFileSize - fileSize:{}", fileSize);
		return fileSize;
	}

}
