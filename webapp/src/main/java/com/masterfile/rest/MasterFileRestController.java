package com.masterfile.rest;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.masterfile.domain.MasterFileVersionVO;
import com.masterfile.service.IFileService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@CrossOrigin
@RestController
@RequestMapping(value = "/masterfile")
@Validated
@Api("MasterFile")
public class MasterFileRestController {

	// Static class logger
	private static final Logger LOGGER = LoggerFactory.getLogger(MasterFileRestController.class);

	@Autowired
	private IFileService fileService;

	@Autowired
	private ExecutorService executorService;

	
	/**
	 * Method to set the executor service to use
	 * @param executorService
	 */
	public void setExecutorService(ExecutorService executorService) {
		this.executorService = executorService;
	}

	/**
	 * Method to save a new File
	 * 
	 * @return
	 */
	@PostMapping("")
	@ApiOperation(value = "Save Master File", notes = "Save a master file version to the database")
	public Future<ResponseEntity<Object>> saveMasterFile(
			@ApiParam(value = "file", required = true, example = "1") @NotNull @RequestParam("file") MultipartFile multiPartFile) {

		return CompletableFuture.supplyAsync(() -> {
			LOGGER.debug("Entered saveMasterFile");
			MasterFileVersionVO savedMasterFileVersion = fileService.saveFile(multiPartFile);
			if (savedMasterFileVersion == null) {
				LOGGER.error("A problem occured saving the masterFile to the database");
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to save File");
			}
			LOGGER.debug("Exiting saveMasterFile - savedMasterFileVersion:{}", savedMasterFileVersion);
			return ResponseEntity.ok().body(savedMasterFileVersion);
		}, executorService);
	}

	/**
	 * Method to update an existing File
	 * 
	 * @return
	 */
	@PostMapping("/{masterFileId}")
	@ApiOperation(value = "Update Master File", notes = "Update a master file version in the database")
	public Future<ResponseEntity<Object>> updateMasterFile(
			@ApiParam(value = "file", required = true, example = "1") @NotNull @RequestParam("file") MultipartFile multiPartFile,
			@ApiParam(value = "masterFileId", required = true, example = "1") @PathVariable("masterFileId") @NotNull final Long masterFileId) {
		LOGGER.debug("Entered updateMasterFile - masterFileId:{}", masterFileId);

		return CompletableFuture.supplyAsync(() -> {
			MasterFileVersionVO updatedMasterFileVersion = fileService.updateFile(multiPartFile, masterFileId);
			if (updatedMasterFileVersion == null) {
				LOGGER.error("A problem occured updating the masterFile with id {} to the database", masterFileId);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to update File with id:"+masterFileId);
			}
			LOGGER.debug("Exiting updateMasterFile");
			return ResponseEntity.ok().body(updatedMasterFileVersion);
		}, executorService);
	}

	/**
	 * Method to get the latest version of all files
	 * 
	 * @return
	 */
	@GetMapping("")
	@ApiOperation(value = "Get All Latest MasterFile Versions", notes = "Retrieves the latest master file version of all files")
	public Future<ResponseEntity<Object>> getAllFiles() {

		return CompletableFuture.supplyAsync(() -> {
			LOGGER.debug("Entered getAllFiles");

			List<MasterFileVersionVO> masterFileVersionList = fileService.getAllFiles();
			if (CollectionUtils.isEmpty(masterFileVersionList)) {
				LOGGER.error("No master file versions returned");
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to get all files");
			}
			LOGGER.debug("Exiting getAllFiles");
			return ResponseEntity.ok().body(masterFileVersionList);
		}, executorService);
	}

	/**
	 * Method to get the latest version of a particular file
	 * 
	 * @return
	 */
	@GetMapping("/{masterFileId}")
	@ApiOperation(value = "Get Latest Master File Version", notes = "Retrieves the latest version of a particular master file")
	public Future<ResponseEntity<Object>> getLatestFileVersion(
			@ApiParam(value = "masterFileId", required = true, example = "1") @PathVariable("masterFileId") @NotNull final Long masterFileId) {

		return CompletableFuture.supplyAsync(() -> {
			LOGGER.debug("Entered getLatestFileVersion - masterFileId:{}", masterFileId);

			MasterFileVersionVO masterFileVersion = fileService.getLatestFileVersion(masterFileId);
			if (masterFileVersion == null) {
				LOGGER.error("Failed to get master file version for the masterFileId:{}", masterFileId);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to get file for the masterFileId:"+masterFileId);
			}
			LOGGER.debug("Exiting getLatestFileVersion");
			return ResponseEntity.ok().body(masterFileVersion);
		}, executorService);
	} 

	/**
	 * Method to get  version of a particular file for a masterFile id and versionId
	 * 
	 * @return
	 */
	@GetMapping("/{masterFileId}/{versionId}")
	@ApiOperation(value = "Get a Master File Version based on masterFileId and versionId", notes = "Retrieves a version of a particular master file based on masterFileId and versionId")
	public Future<ResponseEntity<Object>> getFileVersion(
			@ApiParam(value = "masterFileId", required = true, example = "1") @PathVariable("masterFileId") @NotNull final Long masterFileId,
			@ApiParam(value = "versionId", required = true, example = "1") @PathVariable("versionId") @NotNull final Long versionId) {

		return CompletableFuture.supplyAsync(() -> {
			LOGGER.debug("Entered getFileVersion - masterFileId:{}, versionId:{}", masterFileId, versionId);

			MasterFileVersionVO masterFileVersion = fileService.getFileVersion(masterFileId, versionId);
			if (masterFileVersion == null) {
				LOGGER.error("Failed to get master file version for the masterFileId:{} and versionId:{}", masterFileId, versionId);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to get file for the masterFileId:"+masterFileId+" and versionId:"+versionId);
			}
			LOGGER.debug("Exiting getFileVersion");
			return ResponseEntity.ok().body(masterFileVersion);
		}, executorService);
	} 
	
	/**
	 * Method to get all versions of a particular file
	 * 
	 * @return 
	 */
	@GetMapping("/versions/{masterFileId}")
	@ApiOperation(value = "	Gel All Master File Versions", notes = "Retrieves all master file versions of a particular file")
	public Future<ResponseEntity<Object>> getAllFileVersions(
			@ApiParam(value = "masterFileId", required = true, example = "1") @PathVariable("masterFileId") @NotNull final Long masterFileId) {

		return CompletableFuture.supplyAsync(() -> {
			LOGGER.debug("Entered getAllFileVersions - masterFileId:{}", masterFileId);

			List<MasterFileVersionVO> masterFileVersionList = fileService.getAllFileVersions(masterFileId);
			if (CollectionUtils.isEmpty(masterFileVersionList)) {
				LOGGER.error("No files returned for the masterFileId:{}", masterFileId);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to get all files for the masterFileId:"+masterFileId);
			}
			
			LOGGER.debug("Exiting getAllFileVersions");
			return ResponseEntity.ok().body(masterFileVersionList);
		}, executorService);
	}
	
	/**
	 * Method to delete a particular MasterFile
	 * 
	 * @return
	 */
	@DeleteMapping("/{masterFileId}")
	@ApiOperation(value = "Delete a Master File and all versions", notes = "Delete Master File for a particular Master File Id and all its versions")
	public Future<ResponseEntity<Object>> deleteMasterFile(
			@ApiParam(value = "masterFileId", required = true, example = "1") @PathVariable("masterFileId") @NotNull final Long masterFileId) {

		return CompletableFuture.supplyAsync(() -> {
			LOGGER.debug("Entered deleteMasterFile - masterFileId:{}", masterFileId);

			Boolean result = fileService.deleteMasterFile(masterFileId);
			if (!result) {
				LOGGER.error("Failed to delete master file for the masterFileId:{}", masterFileId);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to delete master file for the masterFileId:"+masterFileId);
			}
			LOGGER.debug("Exiting getLatestFileVersion");
			return ResponseEntity.ok().body(result);
		}, executorService);
	} 
	
	/**
	 * Method to delete a particular MasterFile version
	 * 
	 * @return
	 */
	@DeleteMapping("/{masterFileId}/{versionId}")
	@ApiOperation(value = "Delete a Master File and versions for an id and versionId", notes = "Delete Master File and versions for a particular Master File Id and versionId")
	public Future<ResponseEntity<Object>> deleteMasterFileVersion(
			@ApiParam(value = "masterFileId", required = true, example = "1") @PathVariable("masterFileId") @NotNull final Long masterFileId,
	        @ApiParam(value = "versionId", required = true, example = "1") @PathVariable("versionId") @NotNull final Long versionId){

		return CompletableFuture.supplyAsync(() -> {
			LOGGER.debug("Entered deleteMasterFile - masterFileId:{}, versionId:{}", masterFileId, versionId);

			Boolean result = fileService.deleteMasterFileVersion(masterFileId, versionId);
			if (!result) {
				LOGGER.error("Failed to delete master file for the masterFileId:{} and versionId:{}", masterFileId, versionId);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to delete master file for the masterFileId:"+masterFileId+" and versionId:"+versionId);
			}
			LOGGER.debug("Exiting getLatestFileVersion");
			return ResponseEntity.ok().body(result);
		}, executorService);
	} 
}
