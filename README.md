# Master File Project
## Overview
This project contains all the code for the Master File project.
This project allows users to upload a file, upload new versions, retrieve latest version, retrieve all versions, retrieve a version, delete a version or delete all versions

The files and versions are stored in a MySql database and for testing purposes we use an in memory H2 database.
In a real world scenario, the likes of Amazon S3 can handle storing files and versions but for this particular sample, I just wanted to prove it could be done with a MySql database

## Technologies Used
* Java 8
* Maven
* Spring Boot 
* Docker  + docker-compose
* JUnit
* Spring Test
* Mockito
* MySQL
* H2
* Liquibase
* HikariCP
* Swagger
* JsonPath
* Nginx

## Where technologies are used
I used the latest version of Spring Boot along with Java 8 as I feel it works well.
Docker and Docker compose are used to quickly install all app dependenices and start the application.
As part of the docker setup, we install a MySql image and an Nginx image. Traffic for the application is routed through Nginx and then onto Spring boot
I use Junit, Spring Test and Mockito for testing purposes
The database is setup for the application and integration tests using Liquibase
I use hikariCP as a connection pool for managing total connections to the database
Finally, I use swagger to document the apis and this allows us to easily test the same

## Steps to run
Open the root folder

### Run Application
docker-compose up

Navigate to http://localhost/swagger-ui.html to test out all the requests

### Run Unit and Integration Tests
docker-compose -f docker-compose-tests.yaml up

Results should appear in the console

## Testing API's
It is very important to use the correct id when testing the various api keys.
The id key that is used in all (except Save Master File) is called the masterFileId
This id can be obtained when you save a new file.
The following is an example response you get when saving a new file
```
{
  "dateAdded": "2019-03-15T09:43:26.579",
  "dateUpdated": "2019-03-15T09:43:26.579",
  "id": 4,
  "versionId": 1,
  "isLatestVersion": true,
  "fileName": "lombok.jar",
  "fileType": "application/octet-stream",
  "fileSize": 1718523,
  "masterFile": {
    "dateAdded": "2019-03-15T09:43:26.358",
    "dateUpdated": "2019-03-15T09:43:26.358",
    "id": 2,
    "originalFileName": "lombok.jar",
    "originalFileType": "application/octet-stream",
    "originalFileSize": 1718523,
    "masterFileVersions": []
  }
}
```
**Note:** The max file size that can be uploaded is 256mb and this value can be configured in src/main/resources/application.properties. If you increase this value - please also check the value for client_max_body_size in nginx/conf.d/app.conf
The id that you need to use is called masterFile.id

## DB Structure
Our database is made up of 4 tables


| Name Of Table          | Purpose                                                                                                                                                     |
| ---------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------- |
| DATABASECHANGELOG      | Liquibase managed table                                                                                                                                     |
| DATABASECHANGELOGLOCK  | Liquibase managed table                                                                                                                                     | 
| T_MASTER_FILE          | Used to store new file versions. If you upload a new file, it is stored here first and that id is used to store versions in the T_MASTER_FILE_VERSION table | 
| T_MASTER_FILE_VERSION  | Stores the different versions of files. It has a foreign key to the id of the T_MASTER_FILE table                                                           | 
